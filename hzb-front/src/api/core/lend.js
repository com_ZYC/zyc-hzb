import request from '@/utils/request'
export default {
    // 获得标的列表
    getList() {
        return request({
            url: `/admin/core/lend/list`,
            method: 'get'
        })
    },
    // 根据id查询标的
    show(id) {
        return request({
            url: `/admin/core/lend/show/${id}`,
            method: 'get'
        })
    },
    // 放款
    makeLoan(id) {
        return request({
            url: `/admin/core/lend/makeLoan/${id}`,
            method: 'get'
        })
    }
}