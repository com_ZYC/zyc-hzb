import request from '@/utils/request'
export default {
    // 根据标的id查询标的项列表
    getList(lendId) {
        return request({
            url: `/admin/core/lendItem/list/` + lendId,
            method: 'get'
        })
    }
}