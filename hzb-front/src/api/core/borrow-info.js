import request from '@/utils/request'
export default {
    // 查询借款人列表
    getList() {
        return request({
            url: `/admin/core/borrowInfo/list`,
            method: 'get'
        })
    },
    // 根据 id 查询借款人信息
    show(id) {
        return request({
            url: `/admin/core/borrowInfo/show/${id}`,
            method: 'get'
        })
    },
    // 审批借款
    approval(borrowInfoApproval) {
        return request({
            url: '/admin/core/borrowInfo/approval',
            method: 'post',
            data: borrowInfoApproval
        })
    }
}