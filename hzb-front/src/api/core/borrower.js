import request from '@/utils/request'
export default {
    // 分页查询借款人信息
    getPageList(page, limit, keyword) {
        return request({
            url: `/admin/core/borrower/list/${page}/${limit}`,
            method: 'get',
            params: { keyword }
        })
    },
    // 根据id查询借款人的详细信息
    show(id) {
        return request({
            url: `/admin/core/borrower/show/${id}`,
            method: 'get'
        })
    },
    // 审核借款人信息
    approval(borrowerApproval) {
        return request({
            url: '/admin/core/borrower/approval',
            method: 'post',
            data: borrowerApproval
        })
    }
}