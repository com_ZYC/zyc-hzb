package com.zyc.hzb.sms.controller;

import com.zyc.common.exception.Assert;
import com.zyc.common.result.R;
import com.zyc.common.result.ResponseEnum;
import com.zyc.common.utils.RegexValidateUtils;
import com.zyc.hzb.sms.client.UserInfoClient;
import com.zyc.hzb.sms.service.SmsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 作者:ZYC
 * DATE:2024/8/29
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@RestController("sMSController")
@RequestMapping("/api/sms")
@Api(tags = "短信接口")
public class SMSController {

    @Resource
    private SmsService smsService;
    @Resource
    private UserInfoClient userInfoClient;

    //1. 发送手机验证码
    @ApiOperation("获取验证码")
    @GetMapping("/send/{mobile}")
    public R send(@PathVariable("mobile") String mobile) throws Exception {
        R r = userInfoClient.checkMobile(mobile);
        Assert.isTrue(r.getCode() != -1, ResponseEnum.ERROR);
        boolean isExist = (boolean) r.getData().get("isExist");
        Assert.isTrue(!isExist,ResponseEnum.MOBILE_EXIST_ERROR);
        // 1.1 验证手机号是否为空
        Assert.notNull(mobile, ResponseEnum.MOBILE_NULL_ERROR);
        // 1.2 使用工具类验证手机号
        Assert.isTrue(RegexValidateUtils.checkCellphone(mobile),ResponseEnum.MOBILE_ERROR);
        // 1.3 发送手机验证码
        smsService.send(mobile);
        return R.ok().message("发送验证码成功");
    }
}
