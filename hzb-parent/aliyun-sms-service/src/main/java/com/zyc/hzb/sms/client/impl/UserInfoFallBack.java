package com.zyc.hzb.sms.client.impl;

import com.zyc.common.result.R;
import com.zyc.hzb.sms.client.UserInfoClient;
import org.springframework.stereotype.Component;

@Component
public class UserInfoFallBack implements UserInfoClient {
	@Override
	public R checkMobile(String mobile) {
		return R.error().message("微服务己关闭。");
	}
}