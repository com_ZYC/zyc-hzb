package com.zyc.hzb.sms.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.zyc.common.constant.MyConstant;
import com.zyc.hzb.sms.MyProperties.SmsProperties;
import com.zyc.hzb.sms.service.SmsService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * 作者:ZYC
 * DATE:2024/8/29
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@Service
public class SmsServiceImpl implements SmsService {

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public void send(String mobile) throws Exception {
        // 创建需要发送短信的客户端对象
        Config config = new Config();
        config.setAccessKeyId(SmsProperties.KEY_ID);
        config.setAccessKeySecret(SmsProperties.KEY_SECRET);
        config.endpoint = SmsProperties.END_POINT;
        Client client = new Client(config);

        // 生成短信
        String code = RandomUtil.randomNumbers(6);
        // 短信需要json格式，即键值对，所以先存入map中再转为json格式
        HashMap<String, String> map = new HashMap<>();
        map.put("code", code);

        String key = MyConstant.REDIS_KEY_MOBILE_CODE + mobile;
        // 将短信存入redis中
        redisTemplate.opsForValue().set(key, code, 1, TimeUnit.DAYS);

        // 开始发送短信验证码
        // 1.4 开始发送手机验证码
        // 构造请求对象，请填入请求参数值
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers(mobile)
                .setSignName(SmsProperties.SIGN_NAME)
                .setTemplateCode(SmsProperties.TEMPLATE_CODE)
                .setTemplateParam(JSONUtil.toJsonStr(map));
        // 获取响应对象
        SendSmsResponse sendSmsResponse = client.sendSms(sendSmsRequest);

        // 响应包含服务端响应的 body 和 headers
        System.out.println(JSONUtil.toJsonStr(sendSmsResponse));
    }
}
