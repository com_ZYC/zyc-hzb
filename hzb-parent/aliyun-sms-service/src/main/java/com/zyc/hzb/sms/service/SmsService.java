package com.zyc.hzb.sms.service;

/**
 * 作者:ZYC
 * DATE:2024/8/29
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
public interface SmsService {
    void send(String mobile) throws Exception;
}
