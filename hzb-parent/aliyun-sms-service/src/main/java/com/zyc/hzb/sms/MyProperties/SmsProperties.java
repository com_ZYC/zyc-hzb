package com.zyc.hzb.sms.MyProperties;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "aliyun.sms")
@Data
public class SmsProperties implements InitializingBean {
	private String endpoint;
	private String keyId;
	private String keySecret;
	private String templateCode;
	private String signName;

	public static String END_POINT;
	public static String KEY_ID;
	public static String KEY_SECRET;
	public static String TEMPLATE_CODE;
	public static String SIGN_NAME;

	@Override
	public void afterPropertiesSet() throws Exception {
		END_POINT = endpoint;
		KEY_ID = keyId;
		KEY_SECRET = keySecret;
		TEMPLATE_CODE = templateCode;
		SIGN_NAME = signName;
	}
}