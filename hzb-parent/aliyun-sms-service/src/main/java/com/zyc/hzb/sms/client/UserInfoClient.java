package com.zyc.hzb.sms.client;

import com.zyc.common.result.R;
import com.zyc.hzb.sms.client.impl.UserInfoFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value="service-core",fallback = UserInfoFallBack.class)
public interface UserInfoClient {

	// 1. 验证手机号是否存在
	@GetMapping("/api/core/user/check/{mobile}")
	R checkMobile(@PathVariable("mobile") String mobile);
}