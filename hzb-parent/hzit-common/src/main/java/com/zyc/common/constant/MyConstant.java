package com.zyc.common.constant;

/**
 * 作者:ZYC
 * DATE:2024/8/29
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:女
 */
public class MyConstant {

    public static final String REDIS_KEY_MOBILE_CODE = "hzb:mobile_code:";

    public static final String USER_SEX_MAN = "男";
    public static final String USER_SEX_FEMALE = "女";
    public static final String USER_MARRIED = "已婚";
    public static final String USER_UNMARRIED = "未婚";
    /**
     * 数据字典二级节点的常量字符串
     */
    public static final String DICT_EDUCATION = "education";
    public static final String DICT_MONEYUSE = "moneyUse";
    public static final String DICT_INCOME = "income";
    public static final String DICT_RETURNSOURCE = "returnSource";
    public static final String DICT_RELATION = "relation";
    public static final String DICT_RETURNMETHOD = "returnMethod";

    /**
     * 数据字典再redis中的key
     */
    // 一级key
    public static final String REDIS_DICT_KEY = "hzb:";
    public static final String REDIS_TWO_DICTNODE = "hzb:twoDictNode:";
    public static final String REDIS_TH_DICTNODE = "hzb:thDictNode:";
    // 存放对应二级key的值的key
    public static final String REDIS_DICT_DICTID_KEY = "dictid:";
    public static final String REDIS_DICT_INDUSTRY = "hzb:20000"; // 行业
    public static final String REDIS_DICT_EDUCATION = "hzb:30000"; // 学历
    public static final String REDIS_DICT_INCOME = "hzb:40000"; // 收入
    public static final String REDIS_DICT_RETURNSOURCE = "hzb:50000"; // 收入来源
    public static final String REDIS_DICT_RELATION = "hzb:60000"; // 关系
    public static final String REDIS_DICT_RETURNMETHOD = "hzb:70000"; // 还款方式
    public static final String REDIS_DICT_MONEYUSE = "hzb:80000"; // 资金用途
    public static final String REDIS_DICT_BORROWSTATUS = "hzb:81000"; // 借款状态
    public static final String REDIS_DICT_SCHOOLSTATUS = "hzb:82000"; // 学校性质
}
