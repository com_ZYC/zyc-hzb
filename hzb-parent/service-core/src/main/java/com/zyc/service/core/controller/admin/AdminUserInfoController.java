package com.zyc.service.core.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyc.common.result.R;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.pojo.entity.UserLoginRecord;
import com.zyc.service.core.pojo.vo.UserInfoQuery;
import com.zyc.service.core.service.IUserInfoService;
import com.zyc.service.core.service.IUserLoginRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 作者:ZYC
 * DATE:2024/9/3
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@RestController("adminUserInfoController")
@Api(tags = "会员管理")
@RequestMapping("/admin/core/userInfo")
@Slf4j
@CrossOrigin
public class AdminUserInfoController {
    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private IUserLoginRecordService userLoginRecordService;

    @ApiOperation("获取会员分页列表")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(value = "查询对象")
            UserInfoQuery userInfoQuery) {
        IPage<UserInfo> pageModel = userInfoService.listPage(new Page<>(page, limit), userInfoQuery);
        return R.ok().data("pageModel", pageModel);
    }

    @ApiOperation("锁定和解锁")
    @GetMapping("/lock/{id}/{status}")
    public R lock(
            @ApiParam(value = "用户id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "锁定状态（0：锁定 1：解锁）", required = true)
            @PathVariable("status") Integer status) {
        userInfoService.lock(id, status);
        return R.ok().message(status == 1 ? "解锁成功" : "锁定成功");
    }

    @ApiOperation("获取会员登录日志列表")
    @GetMapping("/listTop50/{pager}/{userId}")
    public R listTop50(
            @ApiParam(value = "用户id", required = true) @PathVariable Long userId,
            @ApiParam(value = "页数", required = true) @PathVariable Integer pager) {
        Page<UserLoginRecord> userLoginRecordList = userLoginRecordService.listTop50(pager,userId);
        return R.ok().data("list", userLoginRecordList);
    }
}
