package com.zyc.service.core.mapper;

import com.zyc.service.core.pojo.entity.UserAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 用户账户 Mapper 接口
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface UserAccountMapper extends BaseMapper<UserAccount> {

    /**
     * 修改用户账户资金
     *
     * @param amount
     * @param bindCode
     * @param freezeAmount
     */
    void updateUserAccount(@Param("amount") BigDecimal amount,
                           @Param("bindCode") String bindCode,
                           @Param("freezeAmount") BigDecimal freezeAmount);
}
