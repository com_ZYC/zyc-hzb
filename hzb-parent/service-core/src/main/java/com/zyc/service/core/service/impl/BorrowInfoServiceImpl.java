package com.zyc.service.core.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.base.utils.JwtUtils;
import com.zyc.common.constant.MyConstant;
import com.zyc.common.exception.Assert;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.BorrowInfoStatusEnum;
import com.zyc.service.core.enums.BorrowerStatusEnum;
import com.zyc.service.core.enums.UserBindEnum;
import com.zyc.service.core.mapper.BorrowerMapper;
import com.zyc.service.core.mapper.IntegralGradeMapper;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.entity.BorrowInfo;
import com.zyc.service.core.mapper.BorrowInfoMapper;
import com.zyc.service.core.pojo.entity.Borrower;
import com.zyc.service.core.pojo.entity.IntegralGrade;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.pojo.vo.BorrowInfoApprovalVO;
import com.zyc.service.core.pojo.vo.BorrowerDetailVO;
import com.zyc.service.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.units.qual.A;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 借款信息表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
@RequiredArgsConstructor
public class BorrowInfoServiceImpl extends ServiceImpl<BorrowInfoMapper, BorrowInfo> implements IBorrowInfoService {

    private final UserInfoMapper userInfoMapper;
    private final IntegralGradeMapper integralGradeMapper;
    private final IDictService dictService;
    private final BorrowerMapper borrowerMapper;
    private final IBorrowerService borrowerService;
    private final ILendService lendService;

    /**
     * 获取该用户借款额度
     *
     * @param request
     * @return
     */
    @Override
    public BigDecimal getBorrowAmount(HttpServletRequest request) {
        // 从请求投中获得token
        String token = request.getHeader("token");
        Assert.isTrue(StrUtil.isNotBlank(token), ResponseEnum.ERROR);
        // 使用jwt工具解析出userid
        Long userId = JwtUtils.getUserId(token);
        // 根据用户id查询用户积分
        UserInfo userInfo = userInfoMapper.selectById(userId);
        Assert.notNull(userInfo, ResponseEnum.LOGIN_MOBILE_ERROR);
        Integer integral = userInfo.getIntegral();
        // 根据当前积分判断当前积分所在等级，并且查询到能够借款的最大额度
        // ge、le 是带等于  gt、lt 不带等于
        IntegralGrade integralGrade = integralGradeMapper.selectOne(
                new LambdaQueryWrapper<IntegralGrade>()
                        .ge(IntegralGrade::getIntegralEnd, integral)
                        .le(IntegralGrade::getIntegralStart, integral)
        );
        // 如果未查询到积分等级对象就返回 0 ，否则就正常返回额度即可
        return integralGrade == null ? new BigDecimal("0") : integralGrade.getBorrowAmount();
    }

    /**
     * 提交借款申请
     *
     * @param borrowInfo
     * @param request
     */
    @Override
    public void saveBorrowInfo(BorrowInfo borrowInfo, HttpServletRequest request) {
        String token = request.getHeader("token");
        Assert.isTrue(StrUtil.isNotBlank(token), ResponseEnum.ERROR);
        // 根据id查询用户数据
        Long userId = JwtUtils.getUserId(token);
        UserInfo userInfo = userInfoMapper.selectById(userId);
        Assert.notNull(userInfo, ResponseEnum.LOGIN_MOBILE_ERROR);
        // 判断用户绑定状态
        Assert.isTrue(
                userInfo.getBindStatus().intValue() == UserBindEnum.BIND_OK.getStatus().intValue(),
                ResponseEnum.USER_NO_BIND_ERROR);
        // 判断用户信息是否审批通过
        Assert.isTrue(
                userInfo.getBorrowAuthStatus().intValue() == BorrowerStatusEnum.AUTH_OK.getStatus().intValue(),
                ResponseEnum.USER_NO_AMOUNT_ERROR);
        // 判断用户的借款金额是否超出额度
        BigDecimal borrowAmount = this.getBorrowAmount(request);
        Assert.isTrue(
                borrowInfo.getAmount().doubleValue() <= borrowAmount.doubleValue(),
                ResponseEnum.USER_AMOUNT_LESS_ERROR);
        // 存储数据
        borrowInfo.setUserId(userId);
        // 百分比转成小数
        borrowInfo.setBorrowYearRate(borrowInfo.getBorrowYearRate().divide(new BigDecimal(100)));
        borrowInfo.setStatus(BorrowInfoStatusEnum.CHECK_RUN.getStatus());   // 状态正在审核中...
        baseMapper.insert(borrowInfo);
    }

    /**
     * 获取借款申请审批状态
     *
     * @param request
     * @return
     */
    @Override
    public Integer getStatusByUserId(HttpServletRequest request) {
        String token = request.getHeader("token");
        Assert.isTrue(StrUtil.isNotBlank(token), ResponseEnum.ERROR);
        Long userId = JwtUtils.getUserId(token);
        BorrowInfo borrowInfo = this.getOne(
                new LambdaQueryWrapper<BorrowInfo>()
                        .eq(BorrowInfo::getUserId, userId));
        return borrowInfo == null ? BorrowInfoStatusEnum.NO_AUTH.getStatus() : borrowInfo.getStatus();
    }

    /**
     * 查询借款信息列表
     *
     * @return
     */
    @Override
    public List<BorrowInfo> selectList() {
        // 自定义sql查询借款信息列表
        List<BorrowInfo> borrowInfoList = baseMapper.selectBorrowInfoList();
        borrowInfoList.forEach(this::getBorrowerInfo);
        return borrowInfoList;
    }

    /**
     * 填充 borrowerInfo 里面的 还款方式、资金用途、用户状态信息
     *
     * @param borrowInfo
     */
    private void getBorrowerInfo(BorrowInfo borrowInfo) {
        String returnMethod = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_RETURNMETHOD, borrowInfo.getReturnMethod());
        String moneyUse = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_MONEYUSE, borrowInfo.getMoneyUse());
        String status = BorrowInfoStatusEnum.getMsgByStatus(borrowInfo.getStatus());
        borrowInfo.getParam().put(MyConstant.DICT_RETURNMETHOD, returnMethod);
        borrowInfo.getParam().put(MyConstant.DICT_MONEYUSE, moneyUse);
        borrowInfo.getParam().put("status", status);
    }

    /**
     * 根据id获取借款信息
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getBorrowInfoDetail(Long id) {
        // 查询借款对象
        BorrowInfo borrowInfo = baseMapper.selectById(id);
        // 组装数据
        getBorrowerInfo(borrowInfo);
        // 根据user_id获取借款人对象
        Borrower borrower = borrowerMapper.selectOne(
                new LambdaQueryWrapper<Borrower>()
                        .eq(Borrower::getUserId, borrowInfo.getUserId())
        );
        // 组装借款人对象
        BorrowerDetailVO borrowerDetailVO = borrowerService.getBorrowerDetailVOById(borrower.getId());
        // 组装数据
        Map<String, Object> result = new HashMap<>();
        result.put("borrowInfo", borrowInfo);
        result.put("borrower", borrowerDetailVO);
        return result;
    }

    /**
     * 审批借款信息
     *
     * @param borrowInfoApprovalVO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approval(BorrowInfoApprovalVO borrowInfoApprovalVO) {
        // 修改借款信息状态
        Long borrowInfoId = borrowInfoApprovalVO.getId();
        BorrowInfo borrowInfo = baseMapper.selectById(borrowInfoId);
        borrowInfo.setStatus(borrowInfoApprovalVO.getStatus());
        baseMapper.updateById(borrowInfo);
        // 审核通过则创建标的
        if (borrowInfoApprovalVO.getStatus().intValue() == BorrowInfoStatusEnum.CHECK_OK.getStatus().intValue()) {
            // 创建标的
            lendService.createLend(borrowInfoApprovalVO, borrowInfo);
        }
    }
}
