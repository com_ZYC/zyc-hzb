package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.LendItemReturn;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借回款记录表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface ILendItemReturnService extends IService<LendItemReturn> {

    /**
     * 根据标的id和用户id查询对应用户的回款计划
     *
     * @param lendId
     * @param userId
     * @return
     */
    List<LendItemReturn> selectByLendId(Long lendId, Long userId);

    /**
     * 封装还款明细
     *
     * @param lendReturnId
     * @return
     */
    List<Map<String, Object>> addReturnDetail(Long lendReturnId);
}
