package com.zyc.service.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.bo.TransFlowBO;
import com.zyc.service.core.pojo.entity.TransFlow;
import com.zyc.service.core.mapper.TransFlowMapper;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.service.ITransFlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 交易流水表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service("transFlowServiceImpl")
@RequiredArgsConstructor
public class TransFlowServiceImpl extends ServiceImpl<TransFlowMapper, TransFlow> implements ITransFlowService {

    private final UserInfoMapper userInfoMapper;

    /**
     * 保留交易流水
     *
     * @param transFlowBO
     */
    @Override
    public void saveTransFlow(TransFlowBO transFlowBO) {
        // 获取用户基本信息 user_info
        UserInfo userInfo = userInfoMapper.selectOne(
                new LambdaQueryWrapper<UserInfo>()
                        .eq(UserInfo::getBindCode, transFlowBO.getBindCode())
        );
        // 存储交易流水数据
        TransFlow transFlow = new TransFlow();
        transFlow.setUserId(userInfo.getId());
        transFlow.setUserName(userInfo.getName());
        transFlow.setTransNo(transFlowBO.getAgentBillNo());
        transFlow.setTransType(transFlowBO.getTransTypeEnum().getTransType());
        transFlow.setTransTypeName(transFlowBO.getTransTypeEnum().getTransTypeName());
        transFlow.setTransAmount(transFlowBO.getAmount());
        transFlow.setMemo(transFlowBO.getMemo());
        save(transFlow);
    }

    /**
     * 判断订单流水是否存在
     *
     * @param agentBillNo
     * @return
     */
    @Override
    public boolean isSaveTransFlow(String agentBillNo) {
        int count = baseMapper.selectCount(
                new LambdaQueryWrapper<TransFlow>()
                        .eq(TransFlow::getTransNo, agentBillNo)
        );
        return count > 0;
    }

    /**
     * 查询指定用户的交易流水
     *
     * @param userId
     * @return
     */
    @Override
    public List<TransFlow> selectByUserId(Long userId) {
        return baseMapper.selectList(
                new LambdaQueryWrapper<TransFlow>()
                        .eq(TransFlow::getUserId, userId)
                        .orderByDesc(TransFlow::getCreateTime)
        );
    }
}
