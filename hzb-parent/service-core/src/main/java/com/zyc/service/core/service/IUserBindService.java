package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.UserBind;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.UserBindVO;

import java.util.Map;

/**
 * <p>
 * 用户绑定表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IUserBindService extends IService<UserBind> {

    /**
     * 用户绑定
     *
     * @param vo
     * @param userId
     * @return
     */
    String userBind(UserBindVO vo, Long userId);

    /**
     * 用户绑定的异步回调函数
     *
     * @param map
     * @return
     */
    String hzbNotify(Map<String, Object> map);

    /**
     * 根据用户id获取bindCode
     *
     * @param userId
     * @return
     */
    String getBindCodeByUserId(Long userId);
}
