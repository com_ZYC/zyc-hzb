package com.zyc.service.core.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyc.common.constant.MyConstant;
import com.zyc.common.exception.Assert;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.BorrowerStatusEnum;
import com.zyc.service.core.enums.IntegralEnum;
import com.zyc.service.core.mapper.BorrowerAttachMapper;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.entity.Borrower;
import com.zyc.service.core.mapper.BorrowerMapper;
import com.zyc.service.core.pojo.entity.BorrowerAttach;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.pojo.entity.UserIntegral;
import com.zyc.service.core.pojo.vo.BorrowerApprovalVO;
import com.zyc.service.core.pojo.vo.BorrowerAttachVO;
import com.zyc.service.core.pojo.vo.BorrowerDetailVO;
import com.zyc.service.core.pojo.vo.BorrowerVO;
import com.zyc.service.core.service.IBorrowerAttachService;
import com.zyc.service.core.service.IBorrowerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.service.IDictService;
import com.zyc.service.core.service.IUserIntegralService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 借款人 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service("borrowerServiceImpl")
public class BorrowerServiceImpl extends ServiceImpl<BorrowerMapper, Borrower> implements IBorrowerService {

    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private IBorrowerAttachService borrowerAttachService;
    @Resource
    private IUserIntegralService userIntegralService;
    @Resource
    private IDictService dictService;


    /**
     * 保存借款人信息
     *
     * @param borrowerVO
     * @param userId
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBorrowerVOByUserId(BorrowerVO borrowerVO, Long userId) {
        Assert.isTrue(borrowerVO != null && userId != null, ResponseEnum.ERROR);
        // 根据用户id查询用户信息
        UserInfo userInfo = userInfoMapper.selectById(userId);
        // 进行属性拷贝
        Borrower borrower = BeanUtil.copyProperties(borrowerVO, Borrower.class);
        borrower.setUserId(userId);
        borrower.setName(userInfo.getName());
        borrower.setIdCard(userInfo.getIdCard());
        borrower.setMobile(userInfo.getMobile());
        borrower.setStatus(BorrowerStatusEnum.AUTH_RUN.getStatus());// 认证中
        baseMapper.insert(borrower);
        // 保存附件
        List<BorrowerAttach> borrowerAttachList = borrowerVO.getBorrowerAttachList();
        borrowerAttachList.forEach(borrowerAttach -> {
            borrowerAttach.setBorrowerId(borrower.getId());
            borrowerAttachService.save(borrowerAttach);
        });

        // 更新会员状态，更新为认证中
        userInfo.setBorrowAuthStatus(BorrowerStatusEnum.AUTH_RUN.getStatus());
        userInfoMapper.updateById(userInfo);
    }

    /**
     * 获取借款人状态
     *
     * @param userId
     * @return
     */
    @Override
    public Integer getStatusByUserId(Long userId) {
        LambdaQueryWrapper<Borrower> wrapper = new LambdaQueryWrapper<Borrower>()
                .select(Borrower::getStatus)
                .eq(Borrower::getUserId, userId);
        List<Object> objects = baseMapper.selectObjs(wrapper);
        if (objects.isEmpty()) {
            // 借款人尚未提交信息
            return BorrowerStatusEnum.NO_AUTH.getStatus();
        }
        return (Integer) objects.get(0);
    }

    /**
     * 根据 页码、每页记录数、关键字 分页查询借款人列表
     *
     * @param page
     * @param limit
     * @param keyword
     * @return
     */
    @Override
    public HashMap<String, Object> listPage(Long page, Long limit, String keyword) {
        Page<Borrower> p = new Page<>(page, limit);
        LambdaQueryWrapper<Borrower> queryWrapper = new LambdaQueryWrapper<>();
        if (!StrUtil.isBlank(keyword)) {
            // 如果传入了关键字，就作为条件带上
            // 模糊查询匹配 名字、身份证、手机号 最后根据id降序排序
            queryWrapper
                    .like(Borrower::getName, keyword)
                    .or().like(Borrower::getContactsMobile, keyword)
                    .or().like(Borrower::getIdCard, keyword)
                    .orderByDesc(Borrower::getId);
        }
        Page<Borrower> result = this.page(p, queryWrapper);
        HashMap<String, Object> map = new HashMap<>();
        map.put("resultList", result.getRecords());
        map.put("resultTotal", result.getTotal());
        return map;
    }

    /**
     * 借款额度审批
     *
     * @param vo
     */
    @Override
    public void approval(BorrowerApprovalVO vo) {
        Assert.isTrue(vo != null, ResponseEnum.ERROR);
        // 查询借款人的认证状态，并更新
        Long borrowerId = vo.getBorrowerId();
        Borrower borrower = baseMapper.selectById(borrowerId);
        borrower.setStatus(vo.getStatus());
        baseMapper.updateById(borrower);
        // 查询用户信息
        Long userId = borrower.getUserId();
        UserInfo userInfo = userInfoMapper.selectById(userId);
        // 给这个用户创建积分表对应的记录
        UserIntegral userIntegral = new UserIntegral();
        userIntegral.setUserId(userId);
        userIntegral.setIntegral(vo.getInfoIntegral());
        userIntegral.setContent(IntegralEnum.BORROWER_INFO.getMsg());
        // 创建一个集合用来存储要新增至用户积分列表中
        ArrayList<UserIntegral> list = new ArrayList<>();
        list.add(userIntegral);
        // 原有积分+用户信息认证的积分
        int curIntegral = userInfo.getIntegral() + vo.getInfoIntegral();
        if (vo.getIsIdCardOk()) {
            curIntegral += IntegralEnum.BORROWER_IDCARD.getIntegral();
            UserIntegral idCard = new UserIntegral();
            idCard.setUserId(userId);
            idCard.setIntegral(IntegralEnum.BORROWER_IDCARD.getIntegral());
            idCard.setContent(IntegralEnum.BORROWER_IDCARD.getMsg());
            list.add(idCard);
        }
        if (vo.getIsHouseOk()) {
            curIntegral += IntegralEnum.BORROWER_HOUSE.getIntegral();
            UserIntegral house = new UserIntegral();
            house.setUserId(userId);
            house.setIntegral(IntegralEnum.BORROWER_HOUSE.getIntegral());
            house.setContent(IntegralEnum.BORROWER_HOUSE.getMsg());
            list.add(house);
        }
        if (vo.getIsCarOk()) {
            curIntegral += IntegralEnum.BORROWER_CAR.getIntegral();
            UserIntegral car = new UserIntegral();
            car.setUserId(userId);
            car.setIntegral(IntegralEnum.BORROWER_CAR.getIntegral());
            car.setContent(IntegralEnum.BORROWER_CAR.getMsg());
            list.add(car);
        }
        userIntegralService.saveBatch(list);
        userInfo.setIntegral(curIntegral);
        // 修改审核状态
        userInfo.setBorrowAuthStatus(vo.getStatus());
        userInfoMapper.updateById(userInfo);
    }

    /**
     * 获取借款人信息
     *
     * @param id
     * @return
     */
    @Override
    public BorrowerDetailVO getBorrowerDetailVOById(Long id) {
        // 获取借款人信息
        Borrower borrower = this.getById(id);
        // 属性拷贝到借款人详细vo
        BorrowerDetailVO borrowerDetailVO = BeanUtil.copyProperties(borrower, BorrowerDetailVO.class);
        // 设置婚否
        borrowerDetailVO.setMarry(borrower.getMarry() ? MyConstant.USER_MARRIED : MyConstant.USER_UNMARRIED);
        // 设置性别
        borrowerDetailVO.setSex(borrower.getSex() == 1 ? MyConstant.USER_SEX_MAN : MyConstant.USER_SEX_FEMALE);
        // 获得下拉列表框的内容
        // 计算下拉列表选中内容
        String education = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_EDUCATION, borrower.getEducation());
        String industry = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_MONEYUSE, borrower.getIndustry());
        String income = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_INCOME, borrower.getIncome());
        String returnSource = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_RETURNSOURCE, borrower.getReturnSource());
        String contactsRelation = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_RELATION, borrower.getContactsRelation());
        // 设置下拉列表选中内容
        borrowerDetailVO.setEducation(education);
        borrowerDetailVO.setIndustry(industry);
        borrowerDetailVO.setIncome(income);
        borrowerDetailVO.setReturnSource(returnSource);
        borrowerDetailVO.setContactsRelation(contactsRelation);

        // 审批状态
        String status = BorrowerStatusEnum.getMsgByStatus(borrower.getStatus());
        borrowerDetailVO.setStatus(status);
        // 获取附件VO列表，并加入到vo中
        List<BorrowerAttachVO> borrowerAttachVOList = borrowerAttachService.selectBorrowerAttachVOList(id);
        borrowerDetailVO.setBorrowerAttachVOList(borrowerAttachVOList);
        return borrowerDetailVO;
    }
}
