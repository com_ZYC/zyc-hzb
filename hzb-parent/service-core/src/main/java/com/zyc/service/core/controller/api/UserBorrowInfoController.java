package com.zyc.service.core.controller.api;

import com.zyc.base.utils.JwtUtils;
import com.zyc.common.result.R;
import com.zyc.service.core.pojo.entity.BorrowInfo;
import com.zyc.service.core.service.IBorrowInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * 作者:ZYC
 * DATE:2024/9/23
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@Api(tags = "借款用户信息相关接口")
@RestController("userBorrowInfoController")
@RequestMapping("/api/core/borrowInfo")
public class UserBorrowInfoController {

    @Resource
    private IBorrowInfoService borrowInfoService;


    @ApiOperation("获取借款额度")
    @GetMapping("/auth/getBorrowAmount")
    public R getBorrowAmount(HttpServletRequest request) {
        BigDecimal borrowAmount = borrowInfoService.getBorrowAmount(request);
        return R.ok().data("borrowAmount", borrowAmount);
    }

    @ApiOperation("提交借款申请")
    @PostMapping("/auth/save")
    public R save(@RequestBody BorrowInfo borrowInfo, HttpServletRequest request) {
        borrowInfoService.saveBorrowInfo(borrowInfo, request);
        return R.ok().message("提交成功");
    }

    @ApiOperation("获取借款申请审批状态")
    @GetMapping("/auth/getBorrowInfoStatus")
    public R getBorrowerStatus(HttpServletRequest request){
        Integer status = borrowInfoService.getStatusByUserId(request);
        return R.ok().data("borrowInfoStatus", status);
    }


}
