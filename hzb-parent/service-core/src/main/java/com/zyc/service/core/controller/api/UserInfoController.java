package com.zyc.service.core.controller.api;

import com.zyc.base.utils.JwtUtils;
import com.zyc.common.result.R;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.pojo.vo.LoginVO;
import com.zyc.service.core.pojo.vo.RegisterVO;
import com.zyc.service.core.pojo.vo.UserIndexVO;
import com.zyc.service.core.pojo.vo.UserInfoVO;
import com.zyc.service.core.service.IUserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 作者:ZYC
 * DATE:2024/8/27
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@RestController("userInfoController")
@Api(tags = "用户相关接口")
@RequestMapping("/api/core/user")
public class UserInfoController {

    @Resource
    private IUserInfoService userInfoService;

    @GetMapping("/findAllUser")
    @ApiOperation("查询所有用户")
    public R findAllUser() {
        List<UserInfo> list = userInfoService.list();
        return R.ok().data("list", list);
    }

    @PostMapping("/register")
    @ApiOperation("用户注册")
    public R register(@ApiParam(value = "用户注册信息", required = true) @RequestBody RegisterVO vo) {
        userInfoService.register(vo);
        return R.ok().message("注册成功");
    }

    @PostMapping("/login")
    @ApiOperation("用户登录")
    public R login(@ApiParam("用户登录信息") @RequestBody LoginVO vo, HttpServletRequest request) {
        UserInfoVO userInfoVO = userInfoService.login(vo, request.getRemoteAddr());
        return R.ok().data("userInfo", userInfoVO);
    }

    // 令牌校验
    @ApiOperation("校验令牌")
    @GetMapping("/checkToken")
    public R checkToken(@RequestHeader("token") String token) {
        boolean result = JwtUtils.checkToken(token);
        return result ? R.ok() : R.setResult(ResponseEnum.LOGIN_AUTH_ERROR);
    }

    // 校验手机号是否在userInfo表中存在
    @ApiOperation("校验手机号是否存在")
    @GetMapping("/check/{mobile}")
    public R checkMobile(@PathVariable("mobile")
                         @ApiParam(value = "手机号", required = true)
                         String mobile) {
        boolean isExist = userInfoService.checkMobile(mobile);
        return R.ok().data("isExist", isExist);
    }

    @ApiOperation("获取个人空间用户信息")
    @GetMapping("/auth/getIndexUserInfo")
    public R getIndexUserInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        UserIndexVO userIndexVO = userInfoService.getIndexUserInfo(userId);
        return R.ok().data("userIndexVO", userIndexVO);
    }
}
