package com.zyc.service.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.base.utils.JwtUtils;
import com.zyc.common.exception.Assert;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.TransTypeEnum;
import com.zyc.service.core.hfb.FormHelper;
import com.zyc.service.core.hfb.HfbConst;
import com.zyc.service.core.hfb.RequestHelper;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.bo.TransFlowBO;
import com.zyc.service.core.pojo.entity.TransFlow;
import com.zyc.service.core.pojo.entity.UserAccount;
import com.zyc.service.core.mapper.UserAccountMapper;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.service.ITransFlowService;
import com.zyc.service.core.service.IUserAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.service.IUserBindService;
import com.zyc.service.core.utils.LendNoUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户账户 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service("userAccountServiceImpl")
@RequiredArgsConstructor
@Slf4j
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount> implements IUserAccountService {


    private final UserInfoMapper userInfoMapper;
    private final ITransFlowService transFlowService;
    private final IUserBindService userBindService;

    /**
     * 用户充值
     *
     * @param chargeAmt
     * @param request
     * @return
     */
    @Override
    public String commitCharge(BigDecimal chargeAmt, HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        UserInfo userInfo = userInfoMapper.selectById(userId);
        Assert.notNull(userInfo, ResponseEnum.ERROR);
        String bindCode = userInfo.getBindCode();
        // 判断账户绑定状态
        Assert.notEmpty(bindCode, ResponseEnum.USER_NO_BIND_ERROR);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);
        paramMap.put("agentBillNo", LendNoUtils.getNo());
        paramMap.put("bindCode", bindCode);
        paramMap.put("chargeAmt", chargeAmt);
        paramMap.put("feeAmt", new BigDecimal("0"));
        paramMap.put("notifyUrl", HfbConst.RECHARGE_NOTIFY_URL);// 检查常量是否正确
        paramMap.put("returnUrl", HfbConst.RECHARGE_RETURN_URL);
        paramMap.put("timestamp", RequestHelper.getTimestamp());
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);
        // 构建充值自动提交表单
        return FormHelper.buildForm(HfbConst.RECHARGE_URL, paramMap);
    }

    /**
     * 用户充值异步回调
     *
     * @param paramMap
     * @return
     */
    @Override
    public String notifyMthod(Map<String, Object> paramMap) {
        // 2. 在流水表trans_flow中查询是否有记录,有证明发送过异步回调
        String transNo = paramMap.get("agentBillNo") + "";
        TransFlow transFlow = transFlowService.getOne(
                new LambdaQueryWrapper<TransFlow>()
                        .eq(TransFlow::getTransNo, transNo)
        );
        if (transFlow != null) {
            log.info("幂等性成立!");
            return "success";
        }
        // 1. 修改 userAccount表
        String bindCode = paramMap.get("bindCode") + "";
        BigDecimal chargeAmt = new BigDecimal(paramMap.get("chargeAmt") + "");
        // 参数1: 充值金额 参数2:bindCode 参数3:冻结金额
        baseMapper.updateUserAccount(chargeAmt, bindCode, new BigDecimal(0));

        UserInfo userInfo = userInfoMapper.selectOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getBindCode, bindCode));

        // 3. 添加流水记录
        transFlow = new TransFlow();
        transFlow.setMemo("充值");    // 备注
        transFlow.setTransAmount(chargeAmt);    // 交易金额
        transFlow.setTransNo(transNo);  // 交易流水号
        transFlow.setTransType(TransTypeEnum.RECHARGE.getTransType());  // 交易类型
        transFlow.setTransTypeName(TransTypeEnum.getTransTypeName(transFlow.getTransType()));  // 交易名字
        transFlow.setUserId(userInfo.getId());  // 用户id
        transFlow.setUserName(userInfo.getName());// 用户name
        // 4. 保存流水记录
        transFlowService.save(transFlow);
        return "fail";
    }

    /**
     * 当前用户余额
     *
     * @param request
     * @return
     */
    @Override
    public BigDecimal getAccount(HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        return getById(userId).getAmount();
    }

    /**
     * 根据用户id查询用户余额
     *
     * @param userId
     * @return
     */
    @Override
    public BigDecimal getNowAmt(Long userId) {
        UserAccount userAccount = getById(userId);
        Assert.notNull(userAccount, ResponseEnum.ERROR);
        return userAccount.getAmount();
    }

    /**
     * 用户体现
     *
     * @param fetchAmt
     * @param token
     * @return
     */
    @Override
    public String commitWithdraw(BigDecimal fetchAmt, String token) {
        // 解析token获得用户id
        Long userId = JwtUtils.getUserId(token);
        // 账户可用余额充足：当前用户的余额 >= 当前用户的提现金额
        // 获取当前用户的账户余额
        UserAccount userAccount = getById(userId);
        // 判断当前用户是否注册账户
        Assert.notNull(userAccount, ResponseEnum.ERROR);
        BigDecimal amount = userAccount.getAmount();
        // 判断 体现金额是否超过余额
        Assert.isTrue(amount.doubleValue() >= fetchAmt.doubleValue(),
                ResponseEnum.NOT_SUFFICIENT_FUNDS_ERROR);
        // 获得到用户账户的唯一标识 bindCode
        String bindCode = userBindService.getBindCodeByUserId(userId);

        // 开始封装想第三方资金托管平台的参数
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);         // 商户id
        paramMap.put("agentBillNo", LendNoUtils.getWithdrawNo());       // 交易账单号
        paramMap.put("bindCode", bindCode);         // 用户 bindCode
        paramMap.put("fetchAmt", fetchAmt);         // 用户体现金额
        paramMap.put("feeAmt", new BigDecimal(0));      // 手续费
        paramMap.put("notifyUrl", HfbConst.WITHDRAW_NOTIFY_URL);    // 异步回调地址
        paramMap.put("returnUrl", HfbConst.WITHDRAW_RETURN_URL);    // 返回地址
        paramMap.put("timestamp", RequestHelper.getTimestamp());    // 当前实践戳
        String sign = RequestHelper.getSign(paramMap);      // 将参数进行加密，得到加密字符串
        paramMap.put("sign", sign);         // 吧加密字符串也带过去
        // 构建自动提交表单
        return FormHelper.buildForm(HfbConst.WITHDRAW_URL, paramMap);
    }

    /**
     * 用户提现异步回调
     *
     * @param paramMap
     */
    @Override
    public void notifyWithdraw(Map<String, Object> paramMap) {
        // 获得当前交易流水号
        String agentBillNo = (String) paramMap.get("agentBillNo");
        // 去交易流水订单表判断是否存在该交易
        boolean result = transFlowService.isSaveTransFlow(agentBillNo);
        if (result) {
            log.warn("幂等性返回");
            return;
        }
        // 得到用户bindCode
        String bindCode = (String) paramMap.get("bindCode");
        // 获得用户提现金额
        String fetchAmt = (String) paramMap.get("fetchAmt");
        // 根据用户账户修改账户金额
        baseMapper.updateUserAccount(new BigDecimal(fetchAmt).negate(), bindCode, BigDecimal.ZERO);
        // 增加交易流水
        TransFlowBO transFlowBO = new TransFlowBO(
                agentBillNo,
                bindCode,
                new BigDecimal(fetchAmt),
                TransTypeEnum.WITHDRAW,
                "用户提现");
        transFlowService.saveTransFlow(transFlowBO);
    }
}
