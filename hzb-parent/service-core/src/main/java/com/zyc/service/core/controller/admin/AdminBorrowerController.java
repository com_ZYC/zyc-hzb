package com.zyc.service.core.controller.admin;

import com.zyc.common.result.R;
import com.zyc.service.core.pojo.vo.BorrowerApprovalVO;
import com.zyc.service.core.pojo.vo.BorrowerDetailVO;
import com.zyc.service.core.service.IBorrowerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * 作者:ZYC
 * DATE:2024/9/23
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@Api(tags = "借款人信息管理")
@RestController("adminBorrowerController")
@RequestMapping("/admin/core/borrower")
public class AdminBorrowerController {

    @Resource
    private IBorrowerService borrowerService;

    @ApiOperation("获取借款人分页列表")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(value = "查询关键字")
            @RequestParam String keyword) {
        // 这里的@RequestParam其实是可以省略的，但是在目前的swagger版本中（2.9.2）不能省略，
        // 否则默认将没有注解的参数解析为body中的传递的数据
        HashMap<String, Object> map = borrowerService.listPage(page, limit, keyword);
        return R.ok().data("map", map);
    }

    @ApiOperation("借款额度审批")
    @PostMapping("/approval")
    public R approval(@RequestBody BorrowerApprovalVO vo) {
        borrowerService.approval(vo);
        return R.ok().message("审批完成");
    }

    @ApiOperation("获取借款人信息")
    @GetMapping("/show/{id}")
    public R show(
            @ApiParam(value = "借款人id", required = true)
            @PathVariable("id") Long id) {
        BorrowerDetailVO borrowerDetailVO = borrowerService.getBorrowerDetailVOById(id);
        return R.ok().data("borrowerDetailVO", borrowerDetailVO);
    }
}
