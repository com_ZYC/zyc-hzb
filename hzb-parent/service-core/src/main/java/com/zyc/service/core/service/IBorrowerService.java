package com.zyc.service.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zyc.service.core.pojo.entity.Borrower;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.BorrowerApprovalVO;
import com.zyc.service.core.pojo.vo.BorrowerDetailVO;
import com.zyc.service.core.pojo.vo.BorrowerVO;

import java.util.HashMap;

/**
 * <p>
 * 借款人 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IBorrowerService extends IService<Borrower> {

    /**
     * 保存借款人信息
     * @param borrowerVO
     * @param userId
     */
    void saveBorrowerVOByUserId(BorrowerVO borrowerVO, Long userId);

    /**
     * 获取借款人状态
     * @param userId
     * @return
     */
    Integer getStatusByUserId(Long userId);

    /**
     * 根据 页码、每页记录数、关键字 分页查询借款人列表
     * @param page
     * @param limit
     * @param keyword
     * @return
     */
    HashMap<String, Object> listPage(Long page, Long limit, String keyword);

    /**
     * 借款额度审批
     * @param vo
     */
    void approval(BorrowerApprovalVO vo);

    /**
     * 根据id获得借款人信息
     * @param id
     * @return
     */
    BorrowerDetailVO getBorrowerDetailVOById(Long id);
}
