package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.UserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户账户 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IUserAccountService extends IService<UserAccount> {

    /**
     * 用户充值
     *
     * @param chargeAmt
     * @param request
     * @return
     */
    String commitCharge(BigDecimal chargeAmt, HttpServletRequest request);

    /**
     * 用户充值异步回调
     *
     * @param paramMap
     * @return
     */
    String notifyMthod(Map<String, Object> paramMap);

    /**
     * 查询当前用户余额
     *
     * @param request
     * @return
     */
    BigDecimal getAccount(HttpServletRequest request);

    /**
     * 根据用户id查询余额
     *
     * @param userId
     * @return
     */
    BigDecimal getNowAmt(Long userId);

    /**
     * 用户余额提现
     *
     * @param fetchAmt
     * @param token
     * @return
     */
    String commitWithdraw(BigDecimal fetchAmt, String token);

    /**
     * 用户提现异步回调
     *
     * @param paramMap
     */
    void notifyWithdraw(Map<String, Object> paramMap);

}
