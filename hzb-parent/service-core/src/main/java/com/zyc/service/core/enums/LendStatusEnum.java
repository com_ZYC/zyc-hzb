package com.zyc.service.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 作者:ZYC
 * DATE:2024/9/25
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@AllArgsConstructor
@Getter
public enum LendStatusEnum {
    CHECK(0, "待发布"),
    INVEST_RUN(1, "募资中"),
    PAY_RUN(2, "还款中"),
    PAY_OK(3, "已结清"),
    FINISH(4, "结标"),
    CANCEL(-1, "已撤标"),
    OVERDUE(-3, "逾期催收中"),
    BAD_BILL(-2, "坏账"),
    ;

    public static String getMsgByStatus(int status) {
        LendStatusEnum arrObj[] = LendStatusEnum.values();
        for (LendStatusEnum obj : arrObj) {
            if (status == obj.getCode()) {
                return obj.getDesc();
            }
        }
        return "";
    }

    private final int code;
    private final String desc;

}
