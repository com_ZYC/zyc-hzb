package com.zyc.service.core.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyc.base.entity.User;
import com.zyc.base.utils.JwtUtils;
import com.zyc.common.constant.MyConstant;
import com.zyc.common.exception.BusinessException;
import com.zyc.common.utils.RegexValidateUtils;
import com.zyc.service.core.mapper.UserAccountMapper;
import com.zyc.service.core.mapper.UserLoginRecordMapper;
import com.zyc.service.core.pojo.entity.UserAccount;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.entity.UserLoginRecord;
import com.zyc.service.core.pojo.vo.*;
import com.zyc.service.core.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * <p>
 * 用户基本信息 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private UserAccountMapper userAccountMapper;
    @Resource
    private UserLoginRecordMapper userLoginRecordMapper;

    @Override
    public void register(RegisterVO vo) {
        if (vo == null) {
            throw new BusinessException("请先填写数据!");
        }
        String mobile = vo.getMobile();
        if (mobile == null || !RegexValidateUtils.checkCellphone(mobile)) {
            throw new BusinessException("手机号码填写有误");
        }
        UserInfo dbUserInfo = getOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getMobile, mobile));
        if (dbUserInfo != null) {
            throw new BusinessException("改手机号已注册，请登录");
        }
        String password = vo.getPassword();
        if (password == null || password.length() < 6) {
            throw new BusinessException("密码填写有误");
        }
        String key = MyConstant.REDIS_KEY_MOBILE_CODE + mobile;
        String dbCode = (String) redisTemplate.opsForValue().get(key);
        String code = vo.getCode();
        // 此时code如果为null的话，假设dbCode也为null会直接抛异常，不为null的话那也和code的equals不会成立，所有code无需判断为null的情况
        if (dbCode == null || !dbCode.equals(code)) {
            throw new BusinessException("验证码填写错误");
        }
        dbUserInfo = new UserInfo();
        // 满足条件，开始注册
        dbUserInfo.setPassword(SecureUtil.md5(password));
        dbUserInfo.setMobile(mobile);
        dbUserInfo.setUserType(vo.getUserType());
        dbUserInfo.setName("用户" + mobile);
        dbUserInfo.setNickName(dbUserInfo.getName());
        save(dbUserInfo);

        // 注册账户
        UserAccount userAccount = new UserAccount();
        userAccount.setUserId(dbUserInfo.getId());
        userAccountMapper.insert(userAccount);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public UserInfoVO login(LoginVO vo, String remoteAddr) {
        if (vo == null) {
            throw new BusinessException("参数不正确");
        }
        String mobile = vo.getMobile();
        if (mobile == null || !RegexValidateUtils.checkCellphone(mobile)) {
            throw new BusinessException("请检查手机号码");
        }
        UserInfo dbUserInfo = getOne(
                new LambdaQueryWrapper<UserInfo>()
                        .eq(UserInfo::getMobile, mobile)
                        .eq(UserInfo::getUserType, vo.getUserType()));
        if (dbUserInfo == null || dbUserInfo.getStatus().equals(UserInfo.STATUS_LOCKED)) {
            throw new BusinessException("用户不存在或状态有误");
        }
        if (StrUtil.isEmpty(vo.getPassword()) || vo.getPassword().length() < 6 || !dbUserInfo.getPassword().equals(SecureUtil.md5(vo.getPassword()))) {
            throw new BusinessException("用户名或密码不正确");
        }
        // 第二部分：填充数据
        // 2.1 生成token
        String token = JwtUtils.createToken(dbUserInfo.getId(), dbUserInfo.getName());
        // 2.2 构建一个UserInfoVo对象
        UserInfoVO userInfoVO = BeanUtil.copyProperties(dbUserInfo, UserInfoVO.class);
        userInfoVO.setToken(token);
        // 2.3 在用户日志中添加信息
        UserLoginRecord record = new UserLoginRecord();
        record.setUserId(dbUserInfo.getId());
        record.setIp(remoteAddr);
        // 2.4 将其保存到数据库中
        userLoginRecordMapper.insert(record);
        return userInfoVO;
    }

    /**
     * 分页待条件查询用户信息
     *
     * @param userInfoPage
     * @param userInfoQuery
     * @return
     */
    @Override
    public IPage<UserInfo> listPage(Page<UserInfo> userInfoPage, UserInfoQuery userInfoQuery) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        if (userInfoQuery != null) {
            queryWrapper.eq(StrUtil.isNotBlank(userInfoQuery.getMobile()), UserInfo::getMobile, userInfoQuery.getMobile())
                    .eq(userInfoQuery.getUserType() != null, UserInfo::getUserType, userInfoQuery.getUserType())
                    .eq(userInfoQuery.getStatus() != null, UserInfo::getStatus, userInfoQuery.getStatus());
        }
        return page(userInfoPage, queryWrapper);
    }

    @Override
    public void lock(Long id, Integer status) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(id);
        userInfo.setStatus(status);
        baseMapper.updateById(userInfo);
    }

    /**
     * 校验手机号是否存在
     *
     * @return
     */
    @Override
    public boolean checkMobile(String mobile) {
        UserInfo userInfo = this.getOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getMobile, mobile));
        return userInfo != null;
    }

    /**
     * 获取个人空间用户信息
     *
     * @param userId
     * @return
     */
    @Override
    public UserIndexVO getIndexUserInfo(Long userId) {
        // 用户信息
        UserInfo userInfo = baseMapper.selectById(userId);
        // 账户信息
        UserAccount userAccount = userAccountMapper.selectOne(
                new LambdaQueryWrapper<UserAccount>()
                        .eq(UserAccount::getUserId, userId)
        );
        // 登录信息
        UserLoginRecord userLoginRecord = userLoginRecordMapper.selectOne(
                new LambdaQueryWrapper<UserLoginRecord>()
                        .eq(UserLoginRecord::getUserId, userId)
                        .orderByDesc(UserLoginRecord::getCreateTime)
                        .last("limit 1")
        );
        // 组装结果数据
        return getUserIndexVO(userInfo, userAccount, userLoginRecord);
    }

    /**
     * 组装个人中心信息
     *
     * @param userInfo
     * @param userAccount
     * @param userLoginRecord
     * @return
     */
    private static UserIndexVO getUserIndexVO(UserInfo userInfo, UserAccount userAccount, UserLoginRecord userLoginRecord) {
        UserIndexVO userIndexVO = new UserIndexVO();
        userIndexVO.setUserId(userInfo.getId());
        userIndexVO.setUserType(userInfo.getUserType());
        userIndexVO.setName(userInfo.getName());
        userIndexVO.setNickName(userInfo.getNickName());
        userIndexVO.setHeadImg(userInfo.getHeadImg());
        userIndexVO.setBindStatus(userInfo.getBindStatus());
        userIndexVO.setAmount(userAccount.getAmount());
        userIndexVO.setFreezeAmount(userAccount.getFreezeAmount());
        userIndexVO.setLastLoginTime(userLoginRecord.getCreateTime());
        return userIndexVO;
    }

}