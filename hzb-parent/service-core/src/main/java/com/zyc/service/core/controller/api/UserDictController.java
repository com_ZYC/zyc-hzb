package com.zyc.service.core.controller.api;

import com.alibaba.excel.EasyExcel;
import com.zyc.common.exception.BusinessException;
import com.zyc.common.result.R;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.pojo.dto.ExcelDictDTO;
import com.zyc.service.core.pojo.entity.Dict;
import com.zyc.service.core.service.IDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * 作者:ZYC
 * DATE:2024/8/28
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@RestController("userDictController")
@RequestMapping("/api/core/dict")
@Api(tags = "数据字典相关接口")
public class UserDictController {

    @Resource
    private IDictService dictService;

    /**
     * 根据数据字典父节点的节点编码获得其分类下的所有子节点
     *
     * @param dictCode
     * @return
     */
    @GetMapping("/findByDictCode/{dictCode}")
    @ApiOperation("根据 dictCode 获取下级节点")
    public R findByDictCode(@ApiParam(value = "节点编码", required = true) @PathVariable("dictCode") String dictCode) {
        List<Dict> dictList = dictService.findByDictCode(dictCode);
        return R.ok().data("dictList", dictList);
    }


}
