package com.zyc.service.test;

/**
 * 作者:ZYC
 * DATE:2024/10/5
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
public class test2 {

    private test1 t;

    public test2(test1 t) {
        System.out.println("test2");
        this.t = t;
    }
}
