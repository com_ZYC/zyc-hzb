package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.BorrowInfo;
import com.zyc.service.core.pojo.entity.Lend;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.BorrowInfoApprovalVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的准备表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface ILendService extends IService<Lend> {

    /**
     * 创建标的
     *
     * @param borrowInfoApprovalVO
     * @param borrowInfo
     */
    void createLend(BorrowInfoApprovalVO borrowInfoApprovalVO, BorrowInfo borrowInfo);

    /**
     * 查询标的列表
     *
     * @return
     */
    List<Lend> getList();

    /**
     * 获取标的信息
     *
     * @param id
     * @return
     */
    Map<String, Object> getLendDetail(Long id);

    /**
     * 计算投资收益
     *
     * @param invest
     * @param yearRate
     * @param totalMonth
     * @param returnMethod
     * @return
     */
    BigDecimal getInterestCount(BigDecimal invest, BigDecimal yearRate, Integer totalMonth, Integer returnMethod);

    /**
     * 放款
     *
     * @param id
     */
    void makeLoan(Long id);
}
