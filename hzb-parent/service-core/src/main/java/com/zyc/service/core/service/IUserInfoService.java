package com.zyc.service.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.*;

import java.math.BigDecimal;

/**
 * <p>
 * 用户基本信息 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IUserInfoService extends IService<UserInfo> {

    void register(RegisterVO vo);

    UserInfoVO login(LoginVO vo, String remoteAddr);

    IPage<UserInfo> listPage(Page<UserInfo> userInfoPage, UserInfoQuery userInfoQuery);

    void lock(Long id, Integer status);

    boolean checkMobile(String mobile);

    /**
     * 获取个人空间用户信息
     *
     * @param userId
     * @return
     */
    UserIndexVO getIndexUserInfo(Long userId);
}
