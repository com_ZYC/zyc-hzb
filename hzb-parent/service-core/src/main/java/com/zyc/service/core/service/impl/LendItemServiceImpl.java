package com.zyc.service.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.base.utils.JwtUtils;
import com.zyc.common.exception.Assert;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.LendStatusEnum;
import com.zyc.service.core.enums.TransTypeEnum;
import com.zyc.service.core.hfb.FormHelper;
import com.zyc.service.core.hfb.HfbConst;
import com.zyc.service.core.hfb.RequestHelper;
import com.zyc.service.core.mapper.LendMapper;
import com.zyc.service.core.mapper.UserAccountMapper;
import com.zyc.service.core.pojo.bo.TransFlowBO;
import com.zyc.service.core.pojo.entity.Lend;
import com.zyc.service.core.pojo.entity.LendItem;
import com.zyc.service.core.mapper.LendItemMapper;
import com.zyc.service.core.pojo.vo.InvestVO;
import com.zyc.service.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.utils.LendNoUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借记录表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class LendItemServiceImpl extends ServiceImpl<LendItemMapper, LendItem> implements ILendItemService {

    private final ILendService lendService;
    private final IUserAccountService userAccountService;
    private final IUserBindService userBindService;
    private final ITransFlowService transFlowService;
    private final UserAccountMapper userAccountMapper;

    /**
     * 会员投资提交数据
     *
     * @param request
     * @param investVO
     * @return
     */
    @Override
    public String commitInvest(HttpServletRequest request, InvestVO investVO) {
        Assert.notNull(investVO, ResponseEnum.ERROR);
        String token = request.getHeader("token");
        // 从token中解析用户id和用户名
        Long userId = JwtUtils.getUserId(token);
        String userName = JwtUtils.getUserName(token);
        investVO.setInvestName(userName);
        investVO.setInvestUserId(userId);

        // 输入参数校验==========================================
        // 1、首先校验当前标的状态是否为募资中，否则不允许投标
        Lend lend = lendService.getById(investVO.getLendId());
        Assert.isTrue(lend.getStatus() == LendStatusEnum.INVEST_RUN.getCode(), ResponseEnum.LEND_INVEST_ERROR);
        // 2、判断当前标的的已募集金额+当前用户投资金额是否超过了标的总金额，不能超出额度
        BigDecimal userInvestAmt = new BigDecimal(investVO.getInvestAmount()); // 申请投标用户的投标金额
        BigDecimal nowAmt = lend.getInvestAmount().add(userInvestAmt);
        Assert.isTrue(nowAmt.doubleValue() <= lend.getAmount().doubleValue(), ResponseEnum.LEND_FULL_SCALE_ERROR);
        // 3、判断当前用户的账户余额需要大于等于投标金额，否则不允许投标
        BigDecimal userAmt = userAccountService.getNowAmt(userId);
        Assert.isTrue(userAmt.doubleValue() >= userInvestAmt.doubleValue(), ResponseEnum.NOT_SUFFICIENT_FUNDS_ERROR);

        // 创建lendItem记录，在商户平台中生成投资信息
        LendItem lendItem = new LendItem();
        lendItem.setInvestUserId(userId);// 投资人id
        lendItem.setInvestName(userName);// 投资人名字
        String lendItemNo = LendNoUtils.getLendItemNo();
        lendItem.setLendItemNo(lendItemNo); // 投资条目编号（一个Lend对应一个或多个LendItem）
        lendItem.setLendId(investVO.getLendId());// 对应的标的id
        lendItem.setInvestAmount(userInvestAmt); // 此笔投资金额
        lendItem.setLendYearRate(lend.getLendYearRate());// 年化
        lendItem.setInvestTime(LocalDateTime.now()); // 投资时间
        lendItem.setLendStartDate(lend.getLendStartDate()); // 开始时间
        lendItem.setLendEndDate(lend.getLendEndDate()); // 结束时间
        // 预期收益
        BigDecimal expectAmount = lendService.getInterestCount(
                lendItem.getInvestAmount(),
                lendItem.getLendYearRate(),
                lend.getPeriod(),
                lend.getReturnMethod());
        lendItem.setExpectAmount(expectAmount);
        // 实际收益
        lendItem.setRealAmount(new BigDecimal(0));
        lendItem.setStatus(0);// 默认状态：刚刚创建
        save(lendItem);

        // 组装投资相关的参数，提交到汇付宝资金托管平台==========================================
        // 在托管平台同步用户的投资信息，修改用户的账户资金信息==========================================
        // 获取投资人的绑定协议号
        String bindCode = userBindService.getBindCodeByUserId(userId);
        // 获取借款人的绑定协议号
        String benefitBindCode = userBindService.getBindCodeByUserId(lend.getUserId());
        // 封装提交至汇付宝的参数
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);
        paramMap.put("voteBindCode", bindCode);
        paramMap.put("benefitBindCode", benefitBindCode);
        paramMap.put("agentProjectCode", lend.getLendNo());// 项目标号
        paramMap.put("agentProjectName", lend.getTitle());
        // 在资金托管平台上的投资订单的唯一编号，要和lendItemNo保持一致。
        paramMap.put("agentBillNo", lendItemNo);// 订单编号
        paramMap.put("voteAmt", investVO.getInvestAmount());
        paramMap.put("votePrizeAmt", "0");
        paramMap.put("voteFeeAmt", "0");
        paramMap.put("projectAmt", lend.getAmount()); // 标的总金额
        paramMap.put("note", "");
        paramMap.put("notifyUrl", HfbConst.INVEST_NOTIFY_URL); // 检查常量是否正确
        paramMap.put("returnUrl", HfbConst.INVEST_RETURN_URL);
        paramMap.put("timestamp", RequestHelper.getTimestamp());
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);
        // 构建充值自动提交表单
        return FormHelper.buildForm(HfbConst.INVEST_URL, paramMap);
    }

    /**
     * 会员投资异步回调
     *
     * @param paramMap
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void lendItemNotify(Map<String, Object> paramMap) {
        log.info("投标成功");
        // 获取投资编号,商户订单号，根据订单号查询交易流水表中是否有该流水，如果有流水的话就证明已经添加过了，为了保证幂等性直接返回，不存在的话才继续往下执行
        String agentBillNo = (String) paramMap.get("agentBillNo");
        boolean result = transFlowService.isSaveTransFlow(agentBillNo);
        if (result) {
            log.warn("幂等性返回");
            return;
        }
        // 获取用户的绑定协议号
        String bindCode = (String) paramMap.get("voteBindCode");
        String voteAmt = (String) paramMap.get("voteAmt");
        // 修改商户系统中的用户账户金额：余额、冻结金额，套用之前的充值方法，需要注意的是现在的余额是-操作，而不是假操作，所以传入一个负数
        userAccountMapper.updateUserAccount(new BigDecimal(voteAmt).negate(), bindCode, new BigDecimal(voteAmt));
        // 修改投资记录的投资状态改为已支付
        LendItem lendItem = this.getOne(
                new LambdaQueryWrapper<LendItem>()
                        .eq(LendItem::getLendItemNo, agentBillNo)
        );
        lendItem.setStatus(1);// 已支付
        updateById(lendItem);
        // 修改标的信息：投资人数、已投金额
        Long lendId = lendItem.getLendId();
        Lend lend = lendService.getById(lendId);
        lend.setInvestNum(lend.getInvestNum() + 1);
        lend.setInvestAmount(lend.getInvestAmount().add(lendItem.getInvestAmount()));
        lendService.updateById(lend);
        // 新增交易流水
        TransFlowBO transFlowBO = new TransFlowBO(
                agentBillNo,
                bindCode,
                new BigDecimal(voteAmt),
                TransTypeEnum.INVEST_LOCK,
                "投资项目编号：" + lend.getLendNo() + "，项目名称：" + lend.getTitle());
        transFlowService.saveTransFlow(transFlowBO);
    }

    /**
     * 获取标的项列表
     *
     * @param lendId
     * @return
     */
    @Override
    public List<LendItem> selectByLendId(Long lendId) {
        return baseMapper.selectList(
                new LambdaQueryWrapper<LendItem>().eq(LendItem::getLendId, lendId)
        );
    }
}
