package com.zyc.service.core.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.common.constant.MyConstant;
import com.zyc.common.exception.Assert;
import com.zyc.common.exception.BusinessException;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.pojo.dto.ExcelDictDTO;
import com.zyc.service.core.pojo.entity.Dict;
import com.zyc.service.core.mapper.DictMapper;
import com.zyc.service.core.service.IDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.utils.MyReadListener;
import org.checkerframework.checker.units.qual.A;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 数据字典 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

    @Resource
    private DictMapper dictMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * excel读取
     *
     * @param file
     */
    @Override
    public void upload(MultipartFile file) {
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
            // 开始读取文件
            EasyExcel.read(inputStream, ExcelDictDTO.class, new MyReadListener(this)).sheet().doRead();
        } catch (IOException e) {
            throw new BusinessException("文件解析失败");
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new BusinessException("文件解析失败");
            }
        }

    }

    /**
     * 查询所有数据字典数据，导出excel用到
     *
     * @return
     */
    @Override
    public List<ExcelDictDTO> listDictData() {
        List<Dict> list = list();
        return BeanUtil.copyToList(list, ExcelDictDTO.class);
    }

    /**
     * 根据父节点id查找子节点
     *
     * @param pid
     * @return
     */
    @Override
    public List<ExcelDictDTO> listByParentId(Integer pid) {
        // 拼接redis的key，每次从redis中拿，拿不到就查询数据库，并且放入到redis中
        List<Object> values = stringRedisTemplate.opsForHash().values(MyConstant.REDIS_TWO_DICTNODE + pid);
        if (!values.isEmpty()) {
            // 数据不为空
            ArrayList<ExcelDictDTO> list = new ArrayList<>();
            for (Object value : values) {
                ExcelDictDTO bean = JSONUtil.toBean(value.toString(), ExcelDictDTO.class);
                list.add(bean);
            }
            return list;
        }
        // 数据不存在的话，去数据库查询数据
        List<Dict> dictList = dictMapper.selectList(
                new LambdaQueryWrapper<Dict>()
                        .eq(Dict::getParentId, pid)
        );
        // 将查询结果放入redis中
        List<ExcelDictDTO> dictDTOS = BeanUtil.copyToList(dictList, ExcelDictDTO.class);
        dictDTOS.forEach(dto -> {
            Boolean has = hasParentNode(dto.getId());
            dto.setHasChildren(has);
            String jsonStr = JSONUtil.toJsonStr(dto);
            stringRedisTemplate.opsForHash().put(MyConstant.REDIS_TWO_DICTNODE + pid, has ? dto.getDictCode() : dto.getId().toString(), jsonStr);
        });
        return dictDTOS;
    }

    /**
     * 根据数据字典父节点的节点编码获得其分类下的所有子节点
     *
     * @param dictCode
     * @return
     */
    @Override
    public List<Dict> findByDictCode(String dictCode) {
        Assert.isTrue(StrUtil.isNotBlank(dictCode), ResponseEnum.ERROR);
        // 封装查询条件，需要 dictCode 相等的
        LambdaQueryWrapper<Dict> wrapper = new LambdaQueryWrapper<Dict>().eq(Dict::getDictCode, dictCode);
        Dict dict = getOne(wrapper);
        // 如果父节点不存在
        Assert.isTrue(dict != null, ResponseEnum.ERROR);
        // 根据父节点id查询子节点列表返回
        wrapper = new LambdaQueryWrapper<Dict>().eq(Dict::getParentId, dict.getId());
        return list(wrapper);
    }

    /**
     * 根据 dictCode 查询对应的一级数据字典，再根据 value 查询对应的字典信息
     *
     * @param value
     * @param dictCode
     * @return
     */
    @Override
    public String getNameByParentDictCodeAndValue(String dictCode, Integer value) {
        Assert.isTrue(StrUtil.isNotBlank(dictCode) && value != null, ResponseEnum.ERROR);
        Object o = stringRedisTemplate.opsForHash().get(MyConstant.REDIS_TWO_DICTNODE, dictCode);

        // 根据 dictCode 查询是否有这个二级节点
        Dict parentDict = baseMapper.selectOne(
                new LambdaQueryWrapper<Dict>()
                        .eq(Dict::getDictCode, dictCode));
        // 没有的话直接返回空
        if (parentDict == null) {
            return "";
        }
        // 有的话 根据 dictCode 和 value 查询具体的字节点的名字
        Dict dict = baseMapper.selectOne(
                new LambdaQueryWrapper<Dict>()
                        .eq(Dict::getParentId, parentDict.getId())
                        .eq(Dict::getValue, value));
        if (dict == null) {
            return "";
        }
        return dict.getName();
    }

    /**
     * 根据二级节点查询三级节点
     *
     * @param dictCode
     * @return
     */
    @Override
    public List<Dict> twoNodeListByParentId(String dictCode) {
        List<Object> values = stringRedisTemplate.opsForHash().values(MyConstant.REDIS_TH_DICTNODE);
        ArrayList<Dict> dicts = new ArrayList<>();
        if (!values.isEmpty()) {
            // 如果存在
            for (Object value : values) {
                Dict dict = JSONUtil.toBean(value.toString(), Dict.class);
                dicts.add(dict);
            }
            return dicts;
        }
        // 不存在的话就重新查询

        return Collections.emptyList();
    }

    /**
     * 根据父节点id判断是否含有子节点
     */
    public Boolean hasParentNode(Long pid) {
        return dictMapper.selectCount(
                new LambdaQueryWrapper<Dict>()
                        .eq(Dict::getParentId, pid)) > 0;
    }
}
