package com.zyc.service.core.controller.api;

import com.zyc.base.utils.JwtUtils;
import com.zyc.common.result.R;
import com.zyc.service.core.hfb.RequestHelper;
import com.zyc.service.core.pojo.vo.UserBindVO;
import com.zyc.service.core.service.IUserBindService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 作者:ZYC
 * DATE:2024/9/13
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@RestController("userBindController")
@RequestMapping("/api/core/userBind")
@Api(tags = "用户绑定相关接口")
public class UserBindController {

    @Resource
    private IUserBindService userBindService;

    /**
     * 用户绑定
     *
     * @param vo
     * @param token
     * @return
     */
    @PostMapping("/auth/bind")
    @ApiOperation("用户绑定")
    public R authBind(
            @RequestBody
            @ApiParam(value = "用户绑定实体", required = true)
            UserBindVO vo,
            @RequestHeader("token")
            @ApiParam(value = "前端传入的请求头token", required = true)
            String token
    ) {
        // 根据token得到用户id
        Long userId = JwtUtils.getUserId(token);
        // 进行用户绑定
        String formStr = userBindService.userBind(vo, userId);
        return R.ok().data("formStr", formStr);
    }

    /**
     * 用户绑定的异步回调，由hfb调用
     * @param request
     * @return
     */
    @PostMapping("/notify")
    public String notify(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, Object> map = RequestHelper.switchMap(parameterMap);
        return userBindService.hzbNotify(map);
    }
}
