package com.zyc.service.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.common.constant.MyConstant;
import com.zyc.common.exception.Assert;
import com.zyc.common.exception.BusinessException;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.LendStatusEnum;
import com.zyc.service.core.enums.ReturnMethodEnum;
import com.zyc.service.core.enums.TransTypeEnum;
import com.zyc.service.core.hfb.HfbConst;
import com.zyc.service.core.hfb.RequestHelper;
import com.zyc.service.core.mapper.LendItemMapper;
import com.zyc.service.core.mapper.UserAccountMapper;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.bo.TransFlowBO;
import com.zyc.service.core.pojo.entity.*;
import com.zyc.service.core.mapper.LendMapper;
import com.zyc.service.core.pojo.vo.BorrowInfoApprovalVO;
import com.zyc.service.core.pojo.vo.BorrowerDetailVO;
import com.zyc.service.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.utils.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 标的准备表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service("lendServiceImpl")
@RequiredArgsConstructor
@Slf4j
public class LendServiceImpl extends ServiceImpl<LendMapper, Lend> implements ILendService {

    private final IDictService dictService;
    private final IBorrowerService borrowerService;
    private final UserInfoMapper userInfoMapper;
    private final UserAccountMapper userAccountMapper;
    private final ITransFlowService transFlowService;
    private final LendItemMapper lendItemMapper;
    private final ILendReturnService lendReturnService;
    private final ILendItemReturnService lendItemReturnService;


    /**
     * 创建标的
     *
     * @param borrowInfoApprovalVO
     * @param borrowInfo
     */
    @Override
    public void createLend(BorrowInfoApprovalVO borrowInfoApprovalVO, BorrowInfo borrowInfo) {
        Lend lend = new Lend();
        // 设置用户id
        lend.setUserId(borrowInfo.getUserId());
        // 设置借款记录id
        lend.setBorrowInfoId(borrowInfo.getId());
        // 使用工具类生成编号
        lend.setLendNo(LendNoUtils.getLendNo());
        // 设置标的title
        lend.setTitle(borrowInfoApprovalVO.getTitle());
        // 设置标的金额
        lend.setAmount(borrowInfo.getAmount());
        // 设置投资期数
        lend.setPeriod(borrowInfo.getPeriod());
        // 由于涉及到金融业务，全部采用的是BigDecimal进行计算，以免精度丢失
        // 设置年化利率，存进去的时候存的是整数，所以计算的时候需要转为小数
        lend.setLendYearRate(borrowInfoApprovalVO.getLendYearRate().divide(new BigDecimal(100)));
        // 设置服务费率，同样需要将整数转为小数
        lend.setServiceRate(borrowInfoApprovalVO.getServiceRate().divide(new BigDecimal(100)));
        // 设置还款方式
        lend.setReturnMethod(borrowInfo.getReturnMethod());
        // 设置最低投资金额
        lend.setLowestAmount(new BigDecimal(100));
        // 设置现有投资人数，刚初始化标的默认为0
        lend.setInvestNum(0);
        // 设置已投金额，初始化标的默认为0
        lend.setInvestAmount(new BigDecimal(0));
        // 设置发布日期，即为标的创建日期
        lend.setPublishDate(LocalDateTime.now());
        // 设置起息日，并且设定指定的时间格式
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate lendStartDate = LocalDate.parse(borrowInfoApprovalVO.getLendStartDate(), dtf);
        lend.setLendStartDate(lendStartDate);
        // 设置结束日期，即计息日起往后推迟还款月份月数即可
        lend.setLendEndDate(lendStartDate.plusMonths(borrowInfo.getPeriod()));
        // 设置标的描述信息
        lend.setLendInfo(borrowInfoApprovalVO.getLendInfo());
        /*
         * 重点：平台收益 计算
         */
        // 计算月年化，月年化 = 年化率 / 12
        BigDecimal monthRate = lend.getServiceRate().divide(new BigDecimal(12), 8, BigDecimal.ROUND_HALF_DOWN);
        // 计算平台收益 = 标的金额 * 月年化 * 还款月份数
        BigDecimal expectAmount = lend.getAmount().multiply(monthRate).multiply(new BigDecimal(lend.getPeriod()));
        // 预期收益
        lend.setExpectAmount(expectAmount);
        // 实际收益，刚创建标的默认为0
        lend.setRealAmount(new BigDecimal(0));
        // 设置标的状态
        lend.setStatus(LendStatusEnum.INVEST_RUN.getCode());
        // 审核时间
        lend.setCheckTime(LocalDateTime.now());
        // 设置审核管理员
        lend.setCheckAdminId(1L);
        save(lend);
    }

    /**
     * 查询标的列表
     *
     * @return
     */
    @Override
    public List<Lend> getList() {
        List<Lend> lends = list();
        // 循环遍历集合，设置对应的还款方式和还款状态
        lends.forEach(lend -> {
            // 通过传入 dictcode 和 value 查询具体的还款方式，首先是通过查询 dictcode 确定 二级节点信息
            // 再根据 二级节点 的id查询所有 parentId 为二级节点id的数据列表，再根据 value 值确定是哪个还款方式
            String returnMethod = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_RETURNMETHOD, lend.getReturnMethod());
            // 通过枚举值获得到对应的枚举描述
            String status = LendStatusEnum.getMsgByStatus(lend.getStatus());
            // 设置到对应的集合参数中，方便前端显示
            lend.getParam().put(MyConstant.DICT_RETURNMETHOD, returnMethod);
            lend.getParam().put("status", status);
        });
        return lends;
    }

    /**
     * 获取标的信息
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getLendDetail(Long id) {
        // 查询标的对象
        Lend lend = getById(id);
        // 组装数据
        String returnMethod = dictService.getNameByParentDictCodeAndValue(MyConstant.DICT_RETURNMETHOD, lend.getReturnMethod());
        String status = LendStatusEnum.getMsgByStatus(lend.getStatus());
        lend.getParam().put(MyConstant.DICT_RETURNMETHOD, returnMethod);
        lend.getParam().put("status", status);
        // 根据user_id获取借款人对象
        Borrower borrower = borrowerService.getOne(
                new LambdaQueryWrapper<Borrower>()
                        .eq(Borrower::getUserId, lend.getUserId())
        );
        // 获取借款人对象详细信息
        BorrowerDetailVO borrowerDetailVO = borrowerService.getBorrowerDetailVOById(borrower.getId());
        // 组装数据
        Map<String, Object> result = new HashMap<>();
        result.put("lend", lend);
        result.put("borrower", borrowerDetailVO);
        return result;
    }

    /**
     * 计算投资收益
     *
     * @param invest
     * @param yearRate
     * @param totalMonth
     * @param returnMethod
     * @return
     */
    @Override
    public BigDecimal getInterestCount(BigDecimal invest, BigDecimal yearRate, Integer totalMonth, Integer returnMethod) {
        BigDecimal interestCount;
        // 计算总利息
        if (returnMethod == ReturnMethodEnum.ONE.getMethod()) {
            // 等额本息
            interestCount = Amount1Helper.getInterestCount(invest, yearRate, totalMonth);
        } else if (returnMethod == ReturnMethodEnum.TWO.getMethod()) {
            // 等额本金
            interestCount = Amount2Helper.getInterestCount(invest, yearRate, totalMonth);
        } else if (returnMethod == ReturnMethodEnum.THREE.getMethod()) {
            // 按期付息一次还款
            interestCount = Amount3Helper.getInterestCount(invest, yearRate, totalMonth);
        } else {
            // 一次付息还款
            interestCount = Amount4Helper.getInterestCount(invest, yearRate, totalMonth);
        }
        // 不存在的话返回0
        return interestCount == null ? BigDecimal.ZERO : interestCount;
    }

    /**
     * 放款
     *
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void makeLoan(Long id) {
        // 获取标的信息
        Lend lend = getById(id);
        // 判断当前标的是否满标，如果没有满标的化就不允许放款
        Assert.isTrue(lend != null && lend.getInvestAmount().doubleValue() == lend.getAmount().doubleValue(), ResponseEnum.LEND_ACCOUNT_LOW);
        // 放款接口调用，封装参数
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID); // 商户id
        paramMap.put("agentProjectCode", lend.getLendNo()); // 标的编号
        String agentBillNo = LendNoUtils.getLoanNo(); // 放款编号
        paramMap.put("agentBillNo", agentBillNo);
        // 平台收益，放款扣除，借款人借款实际金额 = 借款金额 - 平台收益
        // 月年化 = 年利率 / 12
        BigDecimal monthRate = lend.getServiceRate().divide(new BigDecimal(12), 8, BigDecimal.ROUND_DOWN);
        // 平台实际收益 = 已投金额 * 月年化 * 标的期数
        BigDecimal realAmount = lend.getInvestAmount().multiply(monthRate).multiply(new BigDecimal(lend.getPeriod()));
        paramMap.put("mchFee", realAmount); // 商户手续费(平台实际收益)
        paramMap.put("timestamp", RequestHelper.getTimestamp());  // 实践戳
        // 加密
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);
        log.info("放款参数：{}", JSONObject.toJSONString(paramMap));

        // 第三方平台发送同步远程调用
        JSONObject result = RequestHelper.sendRequest(paramMap, HfbConst.MAKE_LOAD_URL);
        log.info("放款结果：" + result.toJSONString());
        // 如果回调码不是0000,放款失败
        Assert.isTrue("0000".equals(result.getString("resultCode")), ResponseEnum.ERROR);
        // 更新标的信息，标的状态更新为还款中
        lend.setRealAmount(realAmount);
        lend.setStatus(LendStatusEnum.PAY_RUN.getCode());
        lend.setPaymentTime(LocalDateTime.now());
        baseMapper.updateById(lend);

        // 获取借款人信息
        Long userId = lend.getUserId();
        String bindCode = userInfoMapper.selectById(userId).getBindCode();
        // 修改借款人的余额，加上 借款总额 - 平台费用
        BigDecimal voteAmt = result.getBigDecimal("voteAmt");
        userAccountMapper.updateUserAccount(voteAmt, bindCode, BigDecimal.ZERO);
        // 给借款人添加流水
        TransFlowBO transFlowBO = new TransFlowBO();
        transFlowBO.setAgentBillNo(agentBillNo);
        transFlowBO.setAmount(voteAmt);
        transFlowBO.setBindCode(bindCode);
        transFlowBO.setTransTypeEnum(TransTypeEnum.BORROW_BACK);
        transFlowBO.setMemo("借款放款到账，编号：" + lend.getLendNo());
        transFlowService.saveTransFlow(transFlowBO);

        // 获取当前标的的投资人列表，根据标的id和标的状态进行查询
        List<LendItem> lendItemList = lendItemMapper.selectList(
                new LambdaQueryWrapper<LendItem>()
                        .eq(LendItem::getLendId, id)
                        .eq(LendItem::getStatus, 1)
        );

        for (LendItem lendItem : lendItemList) {
            // 修投资人的冻结资金
            BigDecimal investAmount = lendItem.getInvestAmount(); // 投资金额
            bindCode = userInfoMapper.selectById(lendItem.getInvestUserId()).getBindCode();
            userAccountMapper.updateUserAccount(BigDecimal.ZERO, bindCode, investAmount.negate()); // 只修改用户的冻结资金
            // 新增投资人交易流水
            transFlowBO = new TransFlowBO();
            transFlowBO.setAgentBillNo(LendNoUtils.getTransNo()); // 生成唯一表示号
            transFlowBO.setAmount(investAmount);  // 变动资金金额
            transFlowBO.setBindCode(bindCode);
            transFlowBO.setTransTypeEnum(TransTypeEnum.INVEST_UNLOCK);
            transFlowBO.setMemo("借款放款到账，编号：" + lend.getLendNo());
            transFlowService.saveTransFlow(transFlowBO);
        }

        // 放款成功生成借款人还款计划和投资人回款计划
        this.repaymentPlan(lend);
    }

    /**
     * 创建还款计划和回款计划
     *
     * @param lend
     */
    private void repaymentPlan(Lend lend) {
        // 还款计划列表
        ArrayList<LendReturn> returnList = new ArrayList<>();
        // 根据期数生成还款计划，每一期都有一个
        for (int i = 1; i <= lend.getPeriod(); i++) {
            // 创建还款计划对象
            LendReturn lendReturn = new LendReturn();
            lendReturn.setReturnNo(LendNoUtils.getReturnNo());  // 生成交易订单号
            lendReturn.setLendId(lend.getId());                  // 设置标的id
            lendReturn.setBorrowInfoId(lend.getBorrowInfoId());   // 设置借款人id
            lendReturn.setUserId(lend.getUserId());              // 设置还款人id
            lendReturn.setAmount(lend.getAmount());              // 标的金额
            lendReturn.setBaseAmount(lend.getInvestAmount());    // 当前期数的
            lendReturn.setLendYearRate(lend.getLendYearRate());  // 年利率
            lendReturn.setCurrentPeriod(i);   // 当前期数
            lendReturn.setReturnMethod(lend.getReturnMethod());   // 还款方式
            // 说明：还款计划中的这三项 = 回款计划中对应的这三项和：因此需要先生成对应的回款计划
            //          lendReturn.setPrincipal();
            //          lendReturn.setInterest();
            //          lendReturn.setTotal();
            lendReturn.setFee(new BigDecimal(0));
            lendReturn.setReturnDate(lend.getLendStartDate().plusMonths(i)); // 设置指定还款日期，当前月的下一个月开始还款
            lendReturn.setIsOverdue(false);
            // 判断当前期数是否是最后一个月，是的话标识为最后一次还款
            lendReturn.setIsLast(i == lend.getPeriod());
            lendReturn.setStatus(0);
            returnList.add(lendReturn);
        }
        // 批量保存还款款计划
        lendReturnService.saveBatch(returnList);
        // 创建一个map，key为还款期数，value为还款计划id
        Map<Integer, Long> lendReturnMap = returnList.stream().collect(
                Collectors.toMap(LendReturn::getCurrentPeriod, LendReturn::getId)
        );

        //=============获取所有投资者，生成回款计划===================
        // 创建和回款计划列表
        ArrayList<LendItemReturn> lendItemReturnAllList = new ArrayList<>();
        // 获取投资成功的投资记录
        List<LendItem> lendItemList = lendItemMapper.selectList(
                new LambdaQueryWrapper<LendItem>()
                        .eq(LendItem::getLendId, lend.getId())
                        .eq(LendItem::getStatus, 1)
        );
        for (LendItem lendItem : lendItemList) {
            // 创建回款计划列表，每个投资者都对应着一个回款计划，还款计划和回款计划是一对多的关系
            List<LendItemReturn> lendItemReturnList = this.returnInvest(lendItem.getId(), lendReturnMap, lend);
            // 将所有的回款计划都存入一个集合当中，所有的投资者的回款计划
            lendItemReturnAllList.addAll(lendItemReturnList);
        }
        // 更新还款计划中的相关金额数据
        for (LendReturn lendReturn : returnList) {
            BigDecimal sumPrincipal = lendItemReturnAllList.stream()
                    // 过滤条件：当回款计划中的还款计划id == 当前还款计划id的时候
                    .filter(item -> item.getLendReturnId().longValue() == lendReturn.getId().longValue())
                    // 将所有回款计划中计算的每月应收本金相加
                    .map(LendItemReturn::getPrincipal)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal sumInterest = lendItemReturnAllList.stream()
                    .filter(item -> item.getLendReturnId().longValue() == lendReturn.getId().longValue())
                    .map(LendItemReturn::getInterest)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            lendReturn.setPrincipal(sumPrincipal); // 每期还款本金
            lendReturn.setInterest(sumInterest); // 每期还款利息
            lendReturn.setTotal(sumPrincipal.add(sumInterest)); // 每期还款本息
        }
        lendReturnService.updateBatchById(returnList);


    }

    /**
     * 为每一个投资者生成对应的回款计划
     *
     * @param lendItemId
     * @param lendReturnMap
     * @param lend
     * @return
     */
    private List<LendItemReturn> returnInvest(Long lendItemId, Map<Integer, Long> lendReturnMap, Lend lend) {
        // 投标信息
        LendItem lendItem = lendItemMapper.selectById(lendItemId);
        // 投资金额
        BigDecimal amount = lendItem.getInvestAmount();
        // 年化利率
        BigDecimal yearRate = lendItem.getLendYearRate();
        // 投资期数
        int totalMonth = lend.getPeriod();
        Map<Integer, BigDecimal> mapInterest = null;  // 还款期数 -> 利息
        Map<Integer, BigDecimal> mapPrincipal = null; // 还款期数 -> 本金
        // 根据还款方式计算本金和利息，计算结果是一个map，key是当前期数，value是当前期数对应的 本金/利息
        // 这里的包装了类型拆箱还是很有必要的，因为如果是对象类型的话无法使用 ==
        if (lend.getReturnMethod().intValue() == ReturnMethodEnum.ONE.getMethod()) {
            // 利息
            mapInterest = Amount1Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            // 本金
            mapPrincipal = Amount1Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        } else if (lend.getReturnMethod().intValue() == ReturnMethodEnum.TWO.getMethod()) {
            mapInterest = Amount2Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            mapPrincipal = Amount2Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        } else if (lend.getReturnMethod().intValue() == ReturnMethodEnum.THREE.getMethod()) {
            mapInterest = Amount3Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            mapPrincipal = Amount3Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        } else {
            mapInterest = Amount4Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            mapPrincipal = Amount4Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        }
        // 创建回款计划列表
        List<LendItemReturn> lendItemReturnList = new ArrayList<>();
        for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
            Integer currentPeriod = entry.getKey();
            // 根据还款期数获取还款计划的id
            Long lendReturnId = lendReturnMap.get(currentPeriod);
            LendItemReturn lendItemReturn = new LendItemReturn();
            lendItemReturn.setLendReturnId(lendReturnId);  // 标的还款id
            lendItemReturn.setLendItemId(lendItemId);  // 标的项id
            lendItemReturn.setInvestUserId(lendItem.getInvestUserId());  // 出借用户id
            lendItemReturn.setLendId(lendItem.getLendId());  // 标的id
            lendItemReturn.setInvestAmount(lendItem.getInvestAmount()); // 出借金额
            lendItemReturn.setLendYearRate(lend.getLendYearRate()); // 年化利率
            lendItemReturn.setCurrentPeriod(currentPeriod); // 当前的期数
            lendItemReturn.setReturnMethod(lend.getReturnMethod()); // 还款方式 1-等额本息 2-等额本金 3-每月还息一次还本 4-一次还本
            // 最后一次本金计算
            if (!lendItemReturnList.isEmpty() && currentPeriod.intValue() == lend.getPeriod().intValue()) {
                // 最后一期本金 = 本金 - 前几次之和
                BigDecimal sumPrincipal = lendItemReturnList.stream()
                        .map(LendItemReturn::getPrincipal) // 值，用于下一步的累计操作
                        .reduce(BigDecimal.ZERO, BigDecimal::add); // reduce 叠加运算，两个参数，第一个参数是起点，第二个参数是每次累计操作，这里是相加
                // 最后一期应还本金 = 用当前投资人的总投资金额 - 除了最后一期前面期数计算出来的所有的应还本金
                BigDecimal lastPrincipal = lendItem.getInvestAmount().subtract(sumPrincipal);
                lendItemReturn.setPrincipal(lastPrincipal);
            } else {
                lendItemReturn.setPrincipal(mapPrincipal.get(currentPeriod));
            }
            lendItemReturn.setInterest(mapInterest.get(currentPeriod));
            lendItemReturn.setTotal(lendItemReturn.getPrincipal().add(lendItemReturn.getInterest()));
            lendItemReturn.setFee(new BigDecimal("0"));
            lendItemReturn.setReturnDate(lend.getLendStartDate().plusMonths(currentPeriod));
            // 是否逾期，默认未逾期
            lendItemReturn.setIsOverdue(false);
            lendItemReturn.setStatus(0);
            lendItemReturnList.add(lendItemReturn);
        }
        lendItemReturnService.saveBatch(lendItemReturnList);
        return lendItemReturnList;
    }

}
