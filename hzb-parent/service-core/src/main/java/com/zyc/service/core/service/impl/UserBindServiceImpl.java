package com.zyc.service.core.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.common.exception.Assert;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.UserBindEnum;
import com.zyc.service.core.hfb.FormHelper;
import com.zyc.service.core.hfb.HfbConst;
import com.zyc.service.core.hfb.RequestHelper;
import com.zyc.service.core.mapper.UserInfoMapper;
import com.zyc.service.core.pojo.entity.UserBind;
import com.zyc.service.core.mapper.UserBindMapper;
import com.zyc.service.core.pojo.entity.UserInfo;
import com.zyc.service.core.pojo.vo.UserBindVO;
import com.zyc.service.core.service.IUserBindService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户绑定表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
public class UserBindServiceImpl extends ServiceImpl<UserBindMapper, UserBind> implements IUserBindService {

    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * 用户绑定
     *
     * @param vo
     * @param userId
     * @return
     */
    @Override
    public String userBind(UserBindVO vo, Long userId) {
        // 根据userid和idcard查询该身份证号是否存在多个用户
        UserBind userBind = getOne(
                new LambdaQueryWrapper<UserBind>()
                        .eq(UserBind::getIdCard, vo.getIdCard())
                        .ne(UserBind::getUserId, userId)
        );
        // 如果存在的话，就说明该身份证已被绑定
        Assert.isNull(userBind, ResponseEnum.USER_BIND_IDCARD_EXIST_ERROR);
        // 进到这里就说明不存在，userbind为null
        /**
         * 开始绑定
         */
        // 再根据userid查询用户表，看是否存在这个记录，有就更新信息，没有就插入一条数据
        userBind = getOne(new QueryWrapper<UserBind>().eq("user_id", userId));
        if (null == userBind) {
            // 插入一条userinfo
            userBind = BeanUtil.copyProperties(vo, UserBind.class);
            userBind.setUserId(userId);
            userBind.setStatus(UserBindEnum.NO_BIND.getStatus());
            this.save(userBind);
        } else {
            // 更新userbind表中该记录的信息
            BeanUtil.copyProperties(vo, userBind);
            this.updateById(userBind);
        }
        return getFormStr(vo, userId);
    }

    /**
     * 用户绑定的异步回调函数，由hfb进行调用
     *
     * @param map
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String hzbNotify(Map<String, Object> map) {
        if (!RequestHelper.isSignEquals(map)) {
            log.error("验签失败！");
            return "fail";
        }
        // 此处可以收用toString转为字符串，但是如果获得的value为null的话，那么使用tostring就会报空指针异常
        String userIdStr = map.get("agentUserId") + "";
        if (StrUtil.isBlank(userIdStr)) {
            return "fail";
        }
        // 验证成功，开始修改userbind表
        Long userId = new Long(userIdStr);
        // 查询userbind的信息
        UserBind userBind = getOne(new LambdaQueryWrapper<UserBind>().eq(UserBind::getUserId, userId));
        UserInfo userInfo = userInfoMapper.selectById(userId);
        if (null == userBind || null == userInfo) {
            return "fail";
        }
        // userbind信息存在之后，修改旧的userbind信息
        String bindCode = map.get("bindCode") + "";
        userBind.setStatus(UserBindEnum.BIND_OK.getStatus());
        userBind.setBindCode(bindCode);

        // 修改user表信息
        userInfo.setIdCard(userBind.getIdCard());
        userInfo.setBindCode(bindCode);
        userInfo.setBindStatus(UserBindEnum.BIND_OK.getStatus());

        // 统一进行修改操作，不阻塞不锁的操作
        this.updateById(userBind);
        userInfoMapper.updateById(userInfo);
        return "success";
    }

    /**
     * 根据用户id获取bindCode
     *
     * @param userId
     * @return
     */
    @Override
    public String getBindCodeByUserId(Long userId) {
        UserBind userBind = getOne(new LambdaQueryWrapper<UserBind>().eq(UserBind::getUserId, userId));
        Assert.notNull(userBind, ResponseEnum.ERROR);
        return userBind.getBindCode();
    }

    /**
     * 构造生成表单的map集合的参数
     *
     * @param vo
     * @param userId
     * @return
     */
    private static String getFormStr(UserBindVO vo, Long userId) {
        Map<String, Object> userBindParams = new HashMap<>();
        userBindParams.put("agentId", HfbConst.AGENT_ID);        // 给商户分配的唯一标识
        userBindParams.put("agentUserId", userId);               // 用户id
        userBindParams.put("idCard", vo.getIdCard());            // 身份证号
        userBindParams.put("personalName", vo.getName());        // 用户姓名
        userBindParams.put("bankType", vo.getBankType());        // 银行类型
        userBindParams.put("bankNo", vo.getBankNo());            // 银行卡号
        userBindParams.put("mobile", vo.getMobile());            // 手机号
        userBindParams.put("returnUrl", HfbConst.USERBIND_RETURN_URL);               // 同步回调地址
        userBindParams.put("notifyUrl", HfbConst.USERBIND_NOTIFY_URL);               // 异步回调地址
        userBindParams.put("timestamp", RequestHelper.getTimestamp());               // 时间戳
        String sign = RequestHelper.getSign(userBindParams);                         // 生成签名
        userBindParams.put("sign", sign);                        // 验签字符串
        // 1.5 生成表单字符串
        return FormHelper.buildForm(HfbConst.USERBIND_URL, userBindParams);
    }
}
