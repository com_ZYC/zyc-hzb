package com.zyc.service.core.service;

import com.zyc.service.core.pojo.bo.TransFlowBO;
import com.zyc.service.core.pojo.entity.TransFlow;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 交易流水表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface ITransFlowService extends IService<TransFlow> {

    /**
     * 保留交易流水
     *
     * @param transFlowBO
     */
    void saveTransFlow(TransFlowBO transFlowBO);

    /**
     * 判断订单流水是否存在
     *
     * @param agentBillNo
     * @return
     */
    boolean isSaveTransFlow(String agentBillNo);

    /**
     * 查询当前登录用户交易流水记录
     *
     * @param userId
     * @return
     */
    List<TransFlow> selectByUserId(Long userId);
}
