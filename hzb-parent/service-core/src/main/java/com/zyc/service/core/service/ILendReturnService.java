package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.LendReturn;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface ILendReturnService extends IService<LendReturn> {

    /**
     * 获取还款计划列表
     *
     * @param lendId
     * @return
     */
    List<LendReturn> selectByLendId(Long lendId);

    /**
     * 用户还款
     *
     * @param lendReturnId
     * @param userId
     * @return
     */
    String commitReturn(Long lendReturnId, Long userId);

    /**
     * 还款异步回调
     *
     * @param paramMap
     */
    void notifyUrl(Map<String, Object> paramMap);

}
