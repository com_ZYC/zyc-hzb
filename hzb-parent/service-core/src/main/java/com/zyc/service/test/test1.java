package com.zyc.service.test;

import org.springframework.context.annotation.Lazy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * 作者:ZYC
 * DATE:2024/10/5
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
public class test1 {

    private test2 test2;

    public test1(@Lazy test2 test2) {
        System.out.println("test1");
        this.test2 = test2;
    }

    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.toArray(new String[integers.size()]);
        integers.add(1);
        String[] strings =new String[10];
        Arrays.asList(strings);
        HashMap<String, String> map = new HashMap<>(10);
        map.put("1","2");
    }

}
