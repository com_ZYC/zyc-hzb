package com.zyc.service.core.mapper;

import com.zyc.service.core.pojo.entity.BorrowInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 借款信息表 Mapper 接口
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface BorrowInfoMapper extends BaseMapper<BorrowInfo> {

    /**
     * 查询借款信息列表
     * @return
     */
    List<BorrowInfo> selectBorrowInfoList();
}
