package com.zyc.service.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.common.exception.Assert;
import com.zyc.common.result.ResponseEnum;
import com.zyc.service.core.enums.LendStatusEnum;
import com.zyc.service.core.enums.TransTypeEnum;
import com.zyc.service.core.hfb.FormHelper;
import com.zyc.service.core.hfb.HfbConst;
import com.zyc.service.core.hfb.RequestHelper;
import com.zyc.service.core.mapper.LendItemMapper;
import com.zyc.service.core.mapper.LendMapper;
import com.zyc.service.core.mapper.UserAccountMapper;
import com.zyc.service.core.pojo.bo.TransFlowBO;
import com.zyc.service.core.pojo.entity.*;
import com.zyc.service.core.mapper.LendReturnMapper;
import com.zyc.service.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.utils.LendNoUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
@RequiredArgsConstructor
public class LendReturnServiceImpl extends ServiceImpl<LendReturnMapper, LendReturn> implements ILendReturnService {

    private final IUserAccountService userAccountService;
    private final IUserInfoService userInfoService;
    private final LendMapper lendMapper;
    private final ILendItemReturnService lendItemReturnService;
    private final ITransFlowService transFlowService;
    private final UserAccountMapper userAccountMapper;
    private final LendItemMapper lendItemMapper;

    /**
     * 获取还款计划列表
     *
     * @param lendId
     * @return
     */
    @Override
    public List<LendReturn> selectByLendId(Long lendId) {
        return baseMapper.selectList(new LambdaQueryWrapper<LendReturn>().eq(LendReturn::getLendId, lendId));
    }

    /**
     * 用户还款
     *
     * @param lendReturnId
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String commitReturn(Long lendReturnId, Long userId) {
        // 获取用户当前期数还款记录
        LendReturn lendReturn = getById(lendReturnId);
        // 判断账户余额是否充足
        UserAccount userAccount = userAccountService.getOne(
                new LambdaQueryWrapper<UserAccount>()
                        .eq(UserAccount::getUserId, userId)
        );
        Assert.notNull(userAccount, ResponseEnum.ERROR);
        // 当前期数还款本息和
        BigDecimal total = lendReturn.getTotal();
        // 获取用户当前账户余额
        BigDecimal amount = userAccount.getAmount();
        Assert.isTrue(total.doubleValue() <= amount.doubleValue(), ResponseEnum.ERROR);
        // 获得还款人bindCode
        UserInfo userInfo = userInfoService.getById(userId);
        Assert.notNull(userInfo, ResponseEnum.ERROR);
        String bindCode = userInfo.getBindCode();
        // 获取表标的信息
        Long lendId = lendReturn.getLendId();
        Lend lend = lendMapper.selectById(lendId);

        // 封装向第三方资金托管平台接口请求参数
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID); // 商户id
        // 商户商品名称
        paramMap.put("agentGoodsName", lend.getTitle());
        // 批次号，交易订单号
        paramMap.put("agentBatchNo", lendReturn.getReturnNo());
        // 还款人绑定协议号
        paramMap.put("fromBindCode", bindCode);
        // 还款总额
        paramMap.put("totalAmt", lendReturn.getTotal());
        paramMap.put("note", "");
        // 封装还款明细
        List<Map<String, Object>> lendItemReturnDetailList = lendItemReturnService.addReturnDetail(lendReturnId);
        paramMap.put("data", JSONObject.toJSONString(lendItemReturnDetailList));    // 还款时间
        paramMap.put("voteFeeAmt", new BigDecimal(0));  // 手续费
        paramMap.put("notifyUrl", HfbConst.BORROW_RETURN_NOTIFY_URL);   // 回调地址
        paramMap.put("returnUrl", HfbConst.BORROW_RETURN_RETURN_URL);   // 返回地址
        paramMap.put("timestamp", RequestHelper.getTimestamp());        // 实践戳
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);
        // 构建自动提交表单
        return FormHelper.buildForm(HfbConst.BORROW_RETURN_URL, paramMap);
    }

    /**
     * 还款异步回调
     *
     * @param paramMap
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void notifyUrl(Map<String, Object> paramMap) {
        // 还款编号
        String agentBatchNo = (String) paramMap.get("agentBatchNo");
        // 幂等性校验，判断当前流水是否在交易流水表中存在
        boolean result = transFlowService.isSaveTransFlow(agentBatchNo);
        if (result) {
            log.warn("幂等性返回");
            return;
        }
        // 获取还款数据
        String voteFeeAmt = (String) paramMap.get("voteFeeAmt");
        LendReturn lendReturn = getOne(
                new LambdaQueryWrapper<LendReturn>()
                        .eq(LendReturn::getReturnNo, agentBatchNo)
        );
        // 更新还款状态
        lendReturn.setStatus(1);   // 己归还
        lendReturn.setFee(new BigDecimal(voteFeeAmt)); // 手续费
        lendReturn.setRealReturnTime(LocalDateTime.now()); // 实际还款时间
        updateById(lendReturn);
        // 更新标的信息
        Lend lend = lendMapper.selectById(lendReturn.getLendId());
        // 最后一次还款更新标的状态
        if (lendReturn.getIsLast()) {
            lend.setStatus(LendStatusEnum.PAY_OK.getCode()); // 己结清
            lendMapper.updateById(lend);
        }
        // 借款人账户转出金额
        BigDecimal totalAmt = new BigDecimal((String) paramMap.get("totalAmt"));// 还款金额
        String bindCode = userInfoService.getById(lend.getUserId()).getBindCode();
        userAccountMapper.updateUserAccount(totalAmt.negate(), bindCode, BigDecimal.ZERO);
        // 借款人交易流水
        TransFlowBO transFlowBO = new TransFlowBO(
                agentBatchNo,
                bindCode,
                totalAmt,
                TransTypeEnum.RETURN_DOWN,
                "借款人还款扣减，项目编号：" + lend.getLendNo() + "，项目名称：" + lend.getTitle());
        transFlowService.saveTransFlow(transFlowBO);

        // 获取回款明细
        List<LendItemReturn> lendItemReturnList = lendItemReturnService.list(
                new LambdaQueryWrapper<LendItemReturn>().eq(LendItemReturn::getLendReturnId, lendReturn.getId())
        );
        lendItemReturnList.forEach(item -> {
            // 更新回款状态
            item.setStatus(1);
            item.setRealReturnTime(LocalDateTime.now());
            lendItemReturnService.updateById(item);
            // 更新出借（标的项）信息
            LendItem lendItem = lendItemMapper.selectById(item.getLendItemId());
            lendItem.setRealAmount(item.getInterest()); // 实际收益
            lendItemMapper.updateById(lendItem);
            // 投资账号转入金额
            String investBindCode = userInfoService.getById(item.getInvestUserId()).getBindCode();
            userAccountMapper.updateUserAccount(item.getTotal(), investBindCode, BigDecimal.ZERO);
            // 投资人账号回款交易流水
            TransFlowBO investTransFlowBO = new TransFlowBO(
                    LendNoUtils.getReturnItemNo(),
                    investBindCode,
                    item.getTotal(),
                    TransTypeEnum.INVEST_BACK,
                    "还款到账，项目编号：" + lend.getLendNo() + "，项目名称：" + lend.getTitle());
            transFlowService.saveTransFlow(investTransFlowBO);
        });
    }

}
