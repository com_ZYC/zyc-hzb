package com.zyc.service.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 作者:ZYC
 * DATE:2024/9/27
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@Getter
@AllArgsConstructor
public enum ReturnMethodEnum {
    ONE(1, "等额本息"),
    TWO(2, "等额本金"),
    THREE(3, "每月还息一次还本"),
    FOUR(4, "一次还本还息"),
    ;
    private final int method;
    private final String msg;

}
