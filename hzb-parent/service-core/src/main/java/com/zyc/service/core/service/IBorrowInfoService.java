package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.BorrowInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.BorrowInfoApprovalVO;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 借款信息表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IBorrowInfoService extends IService<BorrowInfo> {

    /**
     * 获取该用户借款额度
     *
     * @param request
     * @return
     */
    BigDecimal getBorrowAmount(HttpServletRequest request);

    /**
     * 提交借款申请
     *
     * @param borrowInfo
     * @param request
     */
    void saveBorrowInfo(BorrowInfo borrowInfo, HttpServletRequest request);

    /**
     * 获取借款申请审批状态
     *
     * @param request
     * @return
     */
    Integer getStatusByUserId(HttpServletRequest request);

    /**
     * 查询借款信息列表
     *
     * @return
     */
    List<BorrowInfo> selectList();

    /**
     * 根据id获取借款信息
     *
     * @param id
     * @return
     */
    Map<String, Object> getBorrowInfoDetail(Long id);

    /**
     * 审批借款信息
     *
     * @param borrowInfoApprovalVO
     */
    void approval(BorrowInfoApprovalVO borrowInfoApprovalVO);



}
