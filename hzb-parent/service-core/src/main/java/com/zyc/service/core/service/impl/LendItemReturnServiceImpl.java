package com.zyc.service.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zyc.service.core.mapper.LendItemMapper;
import com.zyc.service.core.mapper.LendMapper;
import com.zyc.service.core.mapper.LendReturnMapper;
import com.zyc.service.core.pojo.entity.Lend;
import com.zyc.service.core.pojo.entity.LendItem;
import com.zyc.service.core.pojo.entity.LendItemReturn;
import com.zyc.service.core.mapper.LendItemReturnMapper;
import com.zyc.service.core.pojo.entity.LendReturn;
import com.zyc.service.core.service.ILendItemReturnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyc.service.core.service.IUserBindService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 标的出借回款记录表 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
@Service
@RequiredArgsConstructor
public class LendItemReturnServiceImpl extends ServiceImpl<LendItemReturnMapper, LendItemReturn> implements ILendItemReturnService {

    private final LendReturnMapper lendReturnMapper;
    private final LendMapper lendMapper;
    private final LendItemMapper lendItemMapper;
    private final IUserBindService userBindService;

    /**
     * 根据标的id和用户id查询对应用户的回款计划
     *
     * @param lendId
     * @param userId
     * @return
     */
    @Override
    public List<LendItemReturn> selectByLendId(Long lendId, Long userId) {
        return baseMapper.selectList(
                new LambdaQueryWrapper<LendItemReturn>()
                        .eq(LendItemReturn::getLendId, lendId)
                        .eq(LendItemReturn::getInvestUserId, userId)
                        .orderByAsc(LendItemReturn::getCurrentPeriod));
    }

    /**
     * 封装还款明细
     *
     * @param lendReturnId
     * @return
     */
    @Override
    public List<Map<String, Object>> addReturnDetail(Long lendReturnId) {
        // 获取还款记录
        LendReturn lendReturn = lendReturnMapper.selectById(lendReturnId);
        // 获取标的信息
        Lend lend = lendMapper.selectById(lendReturn.getLendId());
        // 根据标的id查询回款计划列表
        List<LendItemReturn> lendItemReturnList = list(
                new LambdaQueryWrapper<LendItemReturn>()
                        .eq(LendItemReturn::getLendReturnId, lendReturnId)
        );
        List<Map<String, Object>> lendItemReturnDetailList = new ArrayList<>();
        // 遍历还款计划列表
        for (LendItemReturn lendItemReturn : lendItemReturnList) {
            // 获得当前回款计划对应得投资人投资情况
            LendItem lendItem = lendItemMapper.selectById(lendItemReturn.getLendItemId());
            // 投资用户bindCode
            String bindCode = userBindService.getBindCodeByUserId(lendItem.getInvestUserId());
            Map<String, Object> map = new HashMap<>();
            // 项目编号
            map.put("agentProjectCode", lend.getLendNo());
            // 出借编号
            map.put("voteBillNo", lendItem.getLendItemNo());
            // 收款人（出借人）
            map.put("toBindCode", bindCode);
            // 还款金额
            map.put("transitAmt", lendItemReturn.getTotal());
            // 还款本金
            map.put("baseAmt", lendItemReturn.getPrincipal());
            // 还款利息
            map.put("benifitAmt", lendItemReturn.getInterest());
            // 商户手续费
            map.put("feeAmt", BigDecimal.ZERO);
            lendItemReturnDetailList.add(map);
        }
        return lendItemReturnDetailList;
    }

}
