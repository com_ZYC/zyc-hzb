package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.LendItem;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.InvestVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借记录表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface ILendItemService extends IService<LendItem> {

    /**
     * 会员投资提交数据
     *
     * @param request
     * @param investVO
     * @return
     */
    String commitInvest(HttpServletRequest request, InvestVO investVO);

    /**
     * 会员投资异步回调
     *
     * @param paramMap
     */
    void lendItemNotify(Map<String, Object> paramMap);

    /**
     * 获取标的项列表
     *
     * @param lendId
     * @return
     */
    List<LendItem> selectByLendId(Long lendId);
}
