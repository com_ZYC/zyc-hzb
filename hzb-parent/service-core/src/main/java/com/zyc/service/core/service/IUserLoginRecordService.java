package com.zyc.service.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyc.service.core.pojo.entity.UserLoginRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户登录记录表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IUserLoginRecordService extends IService<UserLoginRecord> {

    Page<UserLoginRecord> listTop50(Integer pager, Long userId);
}
