package com.zyc.service.core.service;

import com.zyc.service.core.pojo.entity.BorrowerAttach;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zyc.service.core.pojo.vo.BorrowerAttachVO;

import java.util.List;

/**
 * <p>
 * 借款人上传资源表 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-27
 */
public interface IBorrowerAttachService extends IService<BorrowerAttach> {

    /**
     * 根据借款人id查询附件列表
     * @param id
     * @return
     */
    List<BorrowerAttachVO> selectBorrowerAttachVOList(Long id);
}
