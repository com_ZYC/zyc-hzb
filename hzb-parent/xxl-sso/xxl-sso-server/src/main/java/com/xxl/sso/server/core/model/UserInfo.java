package com.xxl.sso.server.core.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("sys_user")
public class UserInfo {

    @TableId(type = IdType.INPUT,value = "id")
    private String userid;
    private String username;
    private String password;
    private String usercode;
}
