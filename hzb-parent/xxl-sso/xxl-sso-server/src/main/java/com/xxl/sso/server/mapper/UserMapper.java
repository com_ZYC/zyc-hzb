package com.xxl.sso.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxl.sso.server.core.model.UserInfo;

/**
 * 作者:ZYC
 * DATE:2024/8/30
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
public interface UserMapper extends BaseMapper<UserInfo> {
}
