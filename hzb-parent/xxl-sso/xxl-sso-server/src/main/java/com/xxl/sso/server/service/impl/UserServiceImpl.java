package com.xxl.sso.server.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxl.sso.server.core.model.UserInfo;
import com.xxl.sso.server.core.result.ReturnT;
import com.xxl.sso.server.mapper.UserMapper;
import com.xxl.sso.server.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserInfo> implements UserService {

    private static List<UserInfo> mockUserList = new ArrayList<>();

    // static {
    //     for (int i = 0; i <5; i++) {
    //         UserInfo userInfo = new UserInfo();
    //         userInfo.setUserid(1000+i);
    //         userInfo.setUsername("user" + (i>0?String.valueOf(i):""));
    //         userInfo.setPassword("123456");
    //         mockUserList.add(userInfo);
    //     }
    // }

    @Override
    public ReturnT<UserInfo> findUser(String username, String password) {

        if (username == null || username.trim().length() == 0) {
            return new ReturnT<UserInfo>(ReturnT.FAIL_CODE, "请输入用户名.");
        }
        if (password == null || password.trim().length() == 0) {
            return new ReturnT<UserInfo>(ReturnT.FAIL_CODE, "请输入密码.");
        }

        // // mock user
        // for (UserInfo mockUser : mockUserList) {
        //     if (mockUser.getUsername().equals(username) && mockUser.getPassword().equals(password)) {
        //         return new ReturnT<UserInfo>(mockUser);
        //     }
        // }

        UserInfo user = getOne(new QueryWrapper<UserInfo>().eq("username", username));
        // 将密码进行md5加密
        String pwd = SecureUtil.md5(password);
        // 查询到用户了之后不为null并且密码正确才放行
        if (user != null && user.getPassword().equals(pwd)) {
            return new ReturnT<UserInfo>(user);
        }
        return new ReturnT<UserInfo>(ReturnT.FAIL_CODE, "用户名或密码不正确.");
    }


}
