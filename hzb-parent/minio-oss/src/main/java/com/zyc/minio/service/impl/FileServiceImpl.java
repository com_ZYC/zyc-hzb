package com.zyc.minio.service.impl;

import cn.hutool.core.util.StrUtil;
import com.zyc.common.exception.BusinessException;
import com.zyc.minio.config.MinioConfig;
import com.zyc.minio.service.FileService;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.errors.*;
import io.minio.messages.DeleteObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * 作者:ZYC
 * DATE:2024/9/19
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@Service("fileServiceImpl")
@Slf4j
public class FileServiceImpl implements FileService {

    @Resource
    private MinioClient minioClient;
    @Resource
    private MinioConfig minioConfig;

    /**
     * 文件上传接口
     *
     * @param file
     * @param module
     * @return
     */
    @Override
    public String upload(MultipartFile file, String module) throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        // 获得原始文件名
        String originalFilename = file.getOriginalFilename();
        // 获得文件后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        // 组装文件名
        String time = module + "/" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String fileName = time + "/" + uuid + suffix;
        // 开始上传
        PutObjectArgs args = PutObjectArgs.builder()
                .bucket(minioConfig.getBucketName()) // bucket名称
                .object(fileName) // 文件名称
                .stream(file.getInputStream(), file.getSize(), -1) // 文件流，上传整个全部的文件
                .contentType(file.getContentType())
                .build();
        minioClient.putObject(args);
        // 返回文件路径
        return minioConfig.getUrl() + "/" + minioConfig.getBucketName() + "/" + fileName;
    }

    /**
     * 删除文件
     * @param url
     */
    @Override
    public void deleteFile(String url) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        if (StrUtil.isBlank(url)) throw new BusinessException("参数错误");
        minioClient.removeObject(RemoveObjectArgs.builder().bucket(minioConfig.getBucketName()).object(url).build());
    }
}
