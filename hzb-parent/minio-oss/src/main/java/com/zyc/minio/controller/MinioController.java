package com.zyc.minio.controller;

import com.zyc.common.result.R;
import com.zyc.minio.service.FileService;
import io.minio.errors.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * 作者:ZYC
 * DATE:2024/9/19
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@RestController
@RequestMapping("/api/oss")
public class MinioController {

    @Resource
    private FileService fileService;

    /**
     * 文件上传接口
     */
    @PostMapping("/file/upload")
    public R upload(MultipartFile file, String module) throws Exception {
        return R.ok().data("url", fileService.upload(file, module));
    }

    /**
     * 删除文件
     *
     * @param url
     * @return
     */
    @DeleteMapping("/file/remove")
    public R removeFile(String url) throws Exception {
        fileService.deleteFile(url);
        return R.ok();
    }
}
