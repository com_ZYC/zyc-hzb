package com.hzb.gate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HZBGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(HZBGatewayApplication.class);
	}
}