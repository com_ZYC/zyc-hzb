package com.zyc.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据字典
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-29
 */
@Data
@TableName("dict")
@ApiModel(value = "Dict对象", description = "数据字典")
public class Dict implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty("id")
    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.INPUT)
    @ColumnWidth(value = 20)
    private Long id;
    @ColumnWidth(value = 20)
    @ExcelProperty("上级id")
    @ApiModelProperty(value = "上级id")
    @TableField("parent_id")
    private Long parentId;
    @ColumnWidth(value = 20)
    @ExcelProperty("名称")
    @ApiModelProperty(value = "名称")
    @TableField("name")
    private String name;
    @ColumnWidth(value = 20)
    @ExcelProperty("值")
    @ApiModelProperty(value = "值")
    @TableField("value")
    private Integer value;
    @ColumnWidth(value = 20)
    @ExcelProperty("编码")
    @ApiModelProperty(value = "编码")
    @TableField("dict_code")
    private String dictCode;
    @ColumnWidth(value = 20)
    @ExcelProperty("创建时间")
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private String createTime;
    @ColumnWidth(value = 20)
    @ExcelProperty("更新时间")
    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private String updateTime;

    @ColumnWidth(value = 20)
    @ExcelProperty("删除标记")
    @ApiModelProperty(value = "删除标记（0:不可用 1:可用）")
    @TableField("is_deleted")
    private Boolean isDeleted;
}
