package com.zyc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-29
 */
@RestController
@RequestMapping("/dict")
public class DictController {

}
