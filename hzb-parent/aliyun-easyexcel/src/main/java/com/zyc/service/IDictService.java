package com.zyc.service;

import com.zyc.entity.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 数据字典 服务类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-29
 */
public interface IDictService extends IService<Dict> {

}
