package com.zyc.service.impl;

import com.zyc.entity.Dict;
import com.zyc.mapper.DictMapper;
import com.zyc.service.IDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据字典 服务实现类
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-29
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

}
