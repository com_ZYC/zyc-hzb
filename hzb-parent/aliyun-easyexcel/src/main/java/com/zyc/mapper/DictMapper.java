package com.zyc.mapper;

import com.zyc.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 数据字典 Mapper 接口
 * </p>
 *
 * @author ZYC帅哥
 * @since 2024-08-29
 */
public interface DictMapper extends BaseMapper<Dict> {

}
