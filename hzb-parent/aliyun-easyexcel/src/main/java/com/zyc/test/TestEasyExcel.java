package com.zyc.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zyc.EasyExcelApplication;
import com.zyc.entity.Dict;
import com.zyc.service.IDictService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者:ZYC
 * DATE:2024/8/29
 * 快捷键:
 * ctrl+alt+l 自动格式化
 * alt+a/w 光标移至行首/行尾
 * alt+s 转换大小写
 * ctrl+f 在本类中查找
 * use:
 */
@SpringBootTest(classes = EasyExcelApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TestEasyExcel extends AnalysisEventListener<Dict> {

    // 定义一次读取的行数
    private static final Integer SIZE = 10;
    private static final Logger log = LoggerFactory.getLogger(TestEasyExcel.class);
    // 定义集合存放读取的数据
    private List<Dict> list = new ArrayList<>();

    @Resource
    private IDictService dictService;

    /**
     * 测试读取数据库并生成为excel文件
     */
    @Test
    public void test01() {
        List<Dict> list = dictService.list();
        // 要写入的文件对象
        File file = new File("d:/test.xlsx");
        EasyExcel.write(file, Dict.class).sheet("test").doWrite(list);
    }

    /**
     * 从文件中读取数据并写入数据库中
     */
    @Test
    public void test02() throws FileNotFoundException {
        File file = new File("d:/test.xlsx");
        FileInputStream inputStream = new FileInputStream(file);
        // 需要指定目标对象的字节码文件，否则会默认读取到LinkedHashMap，会造成类型转换失败`
        EasyExcel.read(file, Dict.class, this).sheet("test").doRead();
    }

    /**
     * 每读到一行触发的事件
     *
     * @param dict
     * @param analysisContext
     */
    @Override
    public void invoke(Dict dict, AnalysisContext analysisContext) {
        log.info(dict.toString());
        list.add(dict);
        if (list.size() >= SIZE) {
            saveDict(list);
            // 清空集合
            list.clear();
        }
    }

    /**
     * 保存数据
     *
     * @param list
     */
    private void saveDict(List<Dict> list) {
        dictService.saveBatch(list);
    }

    /**
     * 读取完成之后出发的函数
     *
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 为了将不满足10行的数据写入数据库中
        saveDict(list);
    }
}
